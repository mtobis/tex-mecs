USER GUIDE TO Tex-MECS
======================

What is MECS?
-------------

MECS is a lightweight framework for controlling ensembles of
computations. It is particularly targeted to large computations 
or large computer controlled experiments. It seeks to
provide 

* ease of use
* minimal or no intrusion into codes
* extensibility
* error detection and recovery
* output file management

MECS works on Linux and OS X. It may work on Windows; this is a design goal but has not been tested.

MECS requires Python 2.5, 2.6 or 2.7 and has no other dependencies.

I MECS Concepts
---------------

There are a number of reasons to run multiple instances of a large
computation. Typically these are intended to explore over ranges of
parameter values of the computation, but sometimes simply differ
only stochastically. (From the point of view of the software, that would be
a single-parameter system with a varying random seed).

In the following, it is assumed that MECS is installed into directory $MECS_NAME.

1 Component
~~~~~~~~~~~

In MECS terms, each phase of such a repeated computation may be
assigned to a Component. For example, in tuning a model,
model execution could be the responsibility of one component, and
evaluating its performance (or "cost") could be another.

It is a design goal that components, such as various models and
costing algorithms, can be included in MECS without difficulty. This
document explains how to do that.

A component is given a unique name, e.g., ComponentFred, and is
specified in a directory of that name under $MECSDIR/Models, or under
a directory specified in the environment as as $MECS_MOD. 

If you are working with an ensemble of a community model, such as CESM
etc., CESM would be a Component. 

Steps required to install a Component are described below in section III.


2 Scheme
~~~~~~~~

The number and nature of Components, and the sequence of computations
performed by them is controlled by a Scheme. The Scheme determines
which steps are performed in what order, and how and when parameters 
are updated.

Many parameter search schemes are discussed in the literature. Only a
few are currently implemented within MECS. Developing a new Scheme
within the framework is relatively straightforward but requires a
deeper understanding of the system and pf Python than is in scope for 
this document.

Schemes are specified under $MECSDIR in a directory of the same name
as the name of the scheme, or under a directory specified as $MECSSCHEMES

3 Ensemble
~~~~~~~~~~

The user specifies a MECS run by specifying an Ensemble.

The Ensemble specification generally contains information required
by the MECS framework and by the Scheme. It is a file, written in JSON
format, linking a set of names to a set of values. The scheme author
is expected to write a scheme-specific tool to assist in the
construction of the specification file, and a scheme-specific user
quide for performing this task. 

The specification directory is normally in the nonvolatile disk, and
may be located anywhere of the users choice. The Ensemble output is
typically in a different directory and is among the items specified
in the specification process.

Steps required to describe an Ensemble for the schemes included in
the MECS distribution are described in Section IV. 

4 Instance
~~~~~~~~~~

MECS Instance is a job in the operating system which runs a
MECS Ensemble, which in turn runs many instances of the
Components in a sequence specified by a Scheme.

It has three phases:

1) premecs phase - set up directories
2) verification phase - ensure that all compoennts have enough information to proceed
3) run loop - running the components

Technically, the MECS program takes an Ensemble specification and a Scheme
specification, along with any specified Component specifications,
and determines that all information needed by the
Scheme is provided in the Ensemble specification. 

Then it delegates execution to the Scheme, repeatedly
calling the Scheme until the Scheme raises a StopMECS exception, which
occurs when the run is complete or an unhandled error condition arises.

This process will run for (potentially) a very long time, mostly in
the background consuming no resources directly, invoking the component
calculations until the computation stops.

MECS Instance is a job in the operating system which runs a
MECS Ensemble, which in turn runs many instances of the
Components in a sequence specified by a Scheme.

5 Multi-Ensemble
~~~~~~~~~~~~~~~~

An Instance (a running ensemble) may itself be a component in a
larger collection of Ensembles. In the present release this is rather crudely
accomplished.

For purposes of nomenclature clarity, a project that
consists either of several Instances, or in Instances nested
within larger Instances, is called a Multi-Ensemble.

6 Generation
~~~~~~~~~~~~

A single pass through the execution loop is called a
Generation. Typically each Component is invoked once per Generation.

NOTE: Experiment, Model

Over the course of development the words "Experiment" and "Model" have
had multiple meanings. To avoid confusion we do not use them in this document.

The terms Component, Scheme, Ensemble, Instance, Generation, and Invocation
have specific technical meanings within MECS and will be capitalized in this
document as a reminder.

II INSTALLATION
---------------

MECS requires Python version 2.5 or 2.6 or 2.7.

Untar MECS.tar into a directory of your choice. Add that directory to
your system path. In your login scripts, set $MECSDIR to that
directory.

Source that script, or log in with a fresh login. Ensure that $MECSDIR
is properly defined.

To test your installation, issue the commands

::

  chdir $MECSDIR
  python testall.py

and ensure that output matches the descriptions. If this fails, ensure
that your default Python is Python2.7, or invoke Python2.7 explicitly.

Note: on TACC platforms, Python 2.7 is the default after issuing

::

  module load python

III WRAPPING A COMPONENT
------------------------

If you have code that you want to run in an ensemble as a Component,
MECS makes this relatively easy. 

A Component must be specified in a directory. The directory name must
start with an uppercase letter followed by a lowercase letter. That
directory is, by default, s subdirectory of $MECSDIR/Models . The
default may be overridden by environment variable $MECSMOD .

The Component directory must contain the executable for the component,
all non-changing files needed by the component, and 

There are three steps:

1) Identify the files needed to run the Component program(s)

Mecs will run the entire Generation within the context of a newly
created directory as current working directory.

It is necessary to copy any files which are specific to the specific
run into this directory. Other files may be copied as well, or
symbolic links made, as is convenient.

A special file or files in the Component directory called "copy*json"
should be created to contain the information MECS needs to set up the
Generation working directory. That file contains, in JSON format, a
list of files to copy or link, by default from the Component
directory.

JSON format is similar to Python dictionary source. However, note that
trailing commas in lists are not acceptable

A simple example is 

::
 $ more copy.json
 {"files": ["ezparab.py"]}

This will tell MECS to copy the file "ezparab.py" into the Generation's current
working directory when the directory is set up.

Several files can be copied as follows

::
 $ more copy.json 
 {
  "files": ["runfile","datafile1","datafile2"]
 }


If the list of files is for symbolic linking only (useful when the
files are large) the copy command may be overridden with "ln"

::
 $ more copy2.json 
 {"op": "ln -s",
  "files": ["runfile","datafile1","datafile2"]
 }

Finally, the destination may be specified

::
 $ more copy.json 
 {"dest": "%%/subdir_name"
  "files": ["subdir_file1","subdir_file2","subdir_file3"]
 }

Note that MECS will do this with any file matching the pattern 
copy*.json . So multiple copies may be done in the setup phase.

Normally files will be copied into a subdirectory of the working
directory. MECS will replace "%%" with the actual working directory.

[Still to do - adding this feature to the backup phase ]


2) Write the scripts necessary to run the program


*1 - Identifying files needed to run the program or programs*

Each Generation of MECS runs in its own working directory. Files which
differ from one run to another must be copied or linked into the working
directory at the beginning of each generation.

It is currently necessary to ensure that different Components not
refer to different files with the same name. However, MECS can cope
with subdirectories of the working directory in the specification.

*2 - Writing Necessary Scripts*

You must write some extension code in the language of your choice. In
many cases this code is very simple, but it can be as complex as suits
your purposes and the requirements of the Components.

As a general matter, each Scheme may require particular extensions
from its Components. In order for a MECS Instance to work, each
specified Component must provide the extensions required by the chosen Scheme.

In practice all Schemes written to date require only "setup", "run", and
"backup" of each Component. These must be executables, and may alternatively be called
"setup.sh" or "setup.py", or simply "setup" and similarly for "run"
and "backup".

Shell scripts, executables and python scripts are handled slightly
differently, so use the appropriate extension. 

It is possible that you may not wish to have any actions occuring in a
given step. This is okay. You may create an empty executable called
"setup.sh" or "setup.py". The easiest way to do this is with the
"touch" command:

::
 touch setup.py

The setup script must perform any preliminaries (if any) needed to run the
Component. 

The "run" script must invoke the Component.  The "run" script should
return 0 on success, or some nonzero status on irretrievable failure

The "backup" script may make copies of results to nonvolatile backup
media. Note that often on high performance media, files are temporary
by default, and any results which are needed for future analysis
should be saved to permanent media.

IV AVAILABLE SCHEMES
--------------------

Four Schemes are currently available:

Proto
Lister
ListerCoster
VFSA

Proto
-----

Proto contains most of the complexity of MECS, but in itself it
achieves little. It simply runs the same code with the same parameters
over and over. It requires one component called "Model".

Other schemes inherit most of their methods from Proto. It is not
normally of interest to end users. However, it can be used with
a maximum repeat count of 2 to ensure repeatability...

For example, see the Ensemble1 directory in the distribution.

To run the example, ensure that the directory TestRes1 does not exist, or remove it or rename it if it does.

Then issue

::
 ./mecs -s Ensemble1

Look in the Ensemble1 directory for ensemble1.jspec to see how to set up a run.

Lister
------

The Lister Scheme uses predetermined values of a parameter file. The
various versions of the parameter file are stored in the Ensemble
Specification Directory in a subdirectory called List and with names
of the format "parms.dat.g.nnnn" where nnnn are sequential
leading-zero numbers starting at 0000. These are copied into the
Generation directory with the default name "parms.dat", which name can
be overriden by an ensemble parameter "parmfnam". (For example, for a
Fortran computation the variable "parmfnam" in the .jspec file would
often be set to "namelist".)

For example, see the Ensemble2 directory in the distribution.

To run the example, ensure that the directory TestRes2 does not exist, or remove it or rename it if it does.

Then issue

::
 ./mecs -s Ensemble2

ListerCoster
------------

The ListerCoster Scheme is very similar to the Lister Scheme. However,
it invokes an extra component, a Coster, which calculates a
performance metric for each Generation. 

It is actually up to you whether to construe the cost function as a separate Component. You may prefer to construe it as part of the main model, and thus include the 
calculation as part of your model run script.

If you use a distinct cost function, you need a separate setup, run, and backup script for that Component as well, and should ensure that it returns a nonzero value
to the operating system on failure.

For example, see the Ensemble6 directory in the distribution.

To run the example, ensure that the directory TestRes6 does not exist, or remove it or rename it if it does.

Then issue

::
 ./mecs -s Ensemble6


VFSA
----

"VFSA" implements the MVFSA algorithm as described eslewhere by Charles
Jackson. This is a Markov chain Monte Carlo method of exploring
parameter space, so it has a decision mechanism based on the
performance of the previous Generation. 

The parameters for each instance are in this case assembled from a
"template" file and a "bounds" file specified in the Vars subdirectory
of the Experiment Specification. The file named "bounds" is in JSON
format and specifies a range for each variable among the parameters.

::
 {
 "xx" : [0,1],
 "yy" : [0,1]
 }

The file "template" is in python template format. Each variable is
specified by the form 

:
 %(variablename)s

so in the example it looks like this:

::
 sigma   0.1
 atarg   0.4
 btarg   0.4
 yy     %(yy)s
 xx     %(xx)s

Our current version of MVFSA inherits from ListerCoster, so it assumes a separate Component for the cost function.

For example, see the TryVFSA directory in the distribution.

To run the example, ensure that the directory TestVFSA does not exist, or remove it or rename it if it does.

Then issue

::
 ./mecs -s TryVFSA

V SPECIFYING AN ENSEMBLE
------------------------

An Ensemble (or Chain) matches a Scheme with one or more Components.

The Ensemble must be specified in an Ensemble directory. Again the
convention of starting with an uppercase character is enforced. 

Within the Ensemble directory is an ensemble specification file
which is given the name of the Ensemble cast to lowercase with the
extension ".jspec". Thus, in the examples, there is an Ensemble
directory called Ensemble1 and within it an Ensemble
specification called Ensemble/ensemble1.jspec

That file specifies most or all the Ensemble-specific
information. However, a given Scheme may require certain more complex
information for an Ensemble. That information is also specified in
the Ensemble directory in a Scheme-specific way. See the
documentation for the Scheme.

This specification is also given in JSON format.

A minimal example looks like this:

::
 {
 "scheme" : "Lister",
 "rundir" : "TestRes2",
 "Model" : "Parab"
 }

"scheme" is required, and specifies the Scheme

"rundir" is required, and specifies a path (if relative, it is
relative to $MECSDIR) for all Generation
directories and typically all MECS output. For large computations it
is usually on volatile media.

Uppercase items are component specifications. In the case of Lister,
only a single Component, called "Model" is needed. Here the Model
"Parab" is associated with the scheme "Lister" 

Some parameters may be set by environment variables. If they are also present
in the Ensemble specification, the latter is used.

The following parameters and environment variables are recognized by mecs:

::
 PARAMETER        EXAMPLE     REQUIRED? ENVIRONMENT     DEFAULT                        MEANING

                              no        $MECSDIR        path to mecs executable        path relative to which relative filenames are found
 "scheme"         "Lister"    yes       n/a             n/a                            which Scheme MECS uses 
 "rundir"         "myOutput"  yes       n/a             n/a                            where output goes
 "MECS_MOD"       "myModels"  no        $MECS_MODDIR    $MECSDIR                       path to models
 "MECS_SCHEMEDIR" "mySchemes" no        $MECS_SCHEMEDIR MECSDIR/Schemes                path to Schemes

Please note that all existing Schemes have a parameter called Maxgen which defaults to a small number (5). This is the maximum number
of iterations MECS will take. Keeping it small is useful in developing an Ensemble; once you are ready to go into production you should
set it to a large enough number to comfortably suit your purposes.

Each Scheme will have additional requirements. Refer to the Scheme documentation.

VI RUNNING A MECS INSTANCE
--------------------------

To simplify design and testing of restarting, the logic within MECS of
starting or continuing an Ensemble is the same. 

Assuming mecs is in your path and Ensemble1 is in your working

::
 mecs Ensemble1

If the ensemble is already running or has completed, mecs will not start again.

If the host machine failed, mecs may incorrectly believe a given ensemble is running because of the 
existence of a file in the ensemble output directory called "RUNNING". In such a case mecs will report a complete
path to that file; to override the duplicate run protection, remove that file.

Starting and Restarting
-----------------------

Because MECS is intended for long-running Ensembles, facility of
starting and restarting is essential. 

There are two cases which need to be accounted for. One is that MECS
itself stops running, possibly because of an outage on the platform on
which it runs. This is the case which is accounted for by the design
described above.

(Schemes which depend on random calculations may or may not implement
exact bit-for-bit repeatability under MECS restart. This requires
saving the state of the python random number generator. Refer to the
Scheme documentation to ensure that it meets your requirements in this regard.)

Designing your Component for Restart
------------------------------------

Another possibility is that MECS continues running, but a Component
fails. Alternatively, perhaps a Component does not run to completion
because supercomputing resources were inadequate for that purpose. 

For example, CAM runs on a maximum of 64 CPUs, and at one time we had
a suite of computations that took 60 hours x 64 CPUs to run, but we
were only allocated 24 hour slices. It was necessary to start or restart each
Generation's computation three times even under normal operations.

Coping with this is necessarily component-specific and to some extent
platform-specific as well. The "run" method is designed to run the
Component to completion or return a failure status if that is
impossible for some reason. 

"Ordinary" resubmissions, and detection of whether the run has reached 
completion, is a Component-specific task. The "run" method of the
Component is expected to return 0 on success, or some nonzero status 
on irretrievable failure. 

If no account is taken for this contingency, a Component will simply
rerun in the present context. For many codes, this amounts to 
a rerun "from the beginning". For small computations this is
perfectly fine.

Some Caveats about Restarting
-----------------------------

Some models are designed to "pick up where they left off". (In our
particular case, we have used CAM3 which is designed to do that, but
which has a small bug on platforms other than NCAR machines. So the
restart script has to make a minor modification to a read-write file 
which points to the latest restart file.)

There are situations that the designer of the "run" method may wish to
account for. If, for example, data needed for restart is corrupted,
there is a danger of an expensive infinite loop, resubmitting the same
job and failing. So it is safest if restarts are contemplated to
detect whether progress was made since the previous submission. Again,
doing this is necessarily a model-specific task. For CAM3, we look for
the latest monthly output data file and save its name. If the job
exits the queue twice in a row with the same last file, we consider
the process "stuck", and leave the "run" routine with an error condition.

On failure, the MECS instance typically shuts down for user intervention.


VII MULTI-ENSEMBLES
-------------------

A near-future expansion of MECS is Ensembles of Ensembles. A crude
version of this facility is now provided with the mmecs command.

Again we use a directory to encapsulate a MECS concept. In this case
we have an Ensemble Specification Directory.

Within this directory is a JSON-format file called "mspec", where some
of the target strings are python template format. So the file may look
like this:

::
 {
 "toprun" : "TestMM2",
 "expname" : "%(expname)s",
 "scheme" : "Lister",
 "maxgen" : "20",
 "rundir" : "%(targ)s",
 "Model" : "Parab",
 "MECS_MOD" : "Models"
 }

Here "toprun" is a directory in which to run all the component
Ensembles. The forms of expname and rundir should be copied
verbatim. The other parameters are those which are the same for all
components. 

This version is designed to work with lists. If there is a List
subdirectory in the Multi-Ensemble specification, it will be divided among
the component Ensembles. 

This should be considered mostly as a
demonstration rather than as a completed feature.

To run, ensure that the directory TestMM2 does not exist, else rename or remove it. 
Also, in the directory XX2, ensure that there are no subdirectories matching "Ch*",
else remove them.

then issue 

::
 ./mmecs XX2

This will create ten separate MECS instances and ten separate Ensembles.


VIII SUMMARY
------------

An Ensemble links a Scheme to one or more Components. An Ensemble
is specified with an Ensemble Specification file which is inside an
Ensemble Specification directory. A Scheme may require additional
information in this directory in a Scheme-specific way.

A Scheme is a strategy for performing multiple versions of a
Generation, each of which may invoke any or all of the
Components. Scheme design requires some understanding of Python and
MECS internals, and is not covered by this document. Each Scheme is
in its own Scheme directory.

A Component is a computation specified by the user. Little knowledge
of MECS and no Python is required to specify a Component. 

The user must specify which
unchanging files need to be copied into a working directory to run
it. (The Scheme will provide the files which change from one
Generation to another.) The user must provide scripts to set-up, run
and back-up each such computation.

A MECS instance reads an Ensemble specification, verifies that
enough information is available, and proceeds to run all the specified
Generations until the Scheme notes completion.

IX LICENSE
----------

License
-------

Lister and MECS are Copyright (c) 2012 Board of Regents of the University of Texas

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the University of Texas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
