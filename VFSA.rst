USER GUIDE TO VFSA
==================

What is VFSA?
-------------

VFSA is a program which will launch multiple instances of a program with different paramaters. It is built on MECS.

MECS is a lightweight framework for controlling ensembles of
computations. It is particularly targeted to large computations 
or large computer controlled experiments. It seeks to
provide 

* ease of use
* minimal or no intrusion into codes
* extensibility
* error detection and recovery
* output file management

VFSA works on Linux and OS X. It may work on Windows but this has not been tested.

VFSA requires Python 2.5, 2.6 or 2.7 and has no other dependencies.

I VFSA Concepts
---------------

VFSA is a Scheme in the MECS framework. (See the MECS documentation for 
more on the Scheme concept.)

There are a number of reasons to run multiple instances of a large
computation. Typically these are intended to explore over ranges of
parameter values of the computation.

VFSA implements a chain with the Mulitple Very Fast Simulated Annealing algorithm
for searching parameter space as described by e.g.,

Sen, M.K., and P.L. Stoffa, 1996, Bayesian inference, Gibbs sampler and uncertainty estimation 
in geophysical inversion, Geophysical Prospecting, 44, 313-350.

I.1 Instance
~~~~~~~~~~~~

A VFSA Instance is a user-invoked process in the operating system which runs a
VFSA Ensemble, which in turn repetedly sets up, runs, and backs up a Component,
each time in a new file context. That process and any processes which it in turn 
invokes constitute the Instance.

The Instance requires an Ensemble Specification and a Component Specification (see below).

A running Instance has three phases

1) premecs phase - set up directories
2) verification phase - ensure that VFSA has enough information to proceed
3) run loop - running the components

Each iteration of the run loop likewise has phases

a) create the context directory for the specific invocation of the executable
b) copy files into the run context directory
c) invoke the executable
d) check for unrecoverable errors and stop VFSA if such an error occurs
e) back up files

The Instance will run for (potentially) a very long time, mostly in
the background consuming no resources directly, invoking the Component
calculations until the computation stops.

I.2 Component
~~~~~~~~~~~~~

In VFSA terms, each phase of such a repeated computation may be
assigned to a Component. For example, in tuning a model,
model execution could be the responsibility of one component, and
evaluating its performance (or "cost") could be another.

It is a design goal that Components, such as various models and
costing algorithms, can be included in VFSA without much difficulty. This
document explains how to do that.

A Component is given a unique name, e.g., ComponentFred, and is
specified in a directory of that name.

If you are working with an ensemble of a community model, such as CESM
etc., informally you would refer to CESM as a component. More precisely, once
would create a directory with various files required to run CESM in the
VFSA context, generally but not necessarily including the executable. This
CESM-related directory would be the CESM Component.
 
Each component has three executables which it must specify:

a) setup
b) run
c) backup

Details are provided  below in section III.2 .


I.3 Ensemble
~~~~~~~~~~~~

The user specifies a VFSA run by creating and invoking an Ensemble specification.

The Ensemble specification contains information required
by the MECS framework and by VFSA. It is a directory specified
by the user; the name is required to have an uppercase letter
as the first character.

Inside that directory is a file linking a set of names to a set of values. 

Additionally, there is a subdirectory called Vars, which contains two files
which specify the varying paramneter ranges and the structure of the parameter file.

Steps required to create an Ensemble for VFSA are described in detail in Section III.3. 

I.4 Generation
~~~~~~~~~~~~~~

A single pass through the execution loop is called a
Generation. Typically each Component is invoked once per Generation.

I.5 Multi-Ensemble
~~~~~~~~~~~~~~~~~~

An Instance (a running ensemble) may itself be a part of a
larger collection of Instances. VFSA supports this.

::

 The terms Component, Scheme, Ensemble, Instance, Generation, and Invocation
 have specific technical meanings within MECS and VFSA, and will be capitalized in this
 document as a reminder.

II INSTALLATION
---------------

VFSA requires Python version 2.5 or 2.6 or 2.7.

Untar MECS.tar into a directory of your choice. Add that directory to
your system path. In your login scripts, set $MECSDIR to that
directory.

Source that script, or log in with a fresh login. Ensure that $MECSDIR
is properly defined.

To test your installation, issue the commands

::

  chdir $MECSDIR
  python testall.py

and ensure that output matches the descriptions. If this fails, ensure
that your default Python is Python 2 sub-version 5 or greater.

*NOTE*

On TACC platforms, Python 2.7 is the default after issuing

::

  module load python


III VFSA IN PRACTICE
--------------------

The minimal steps to run VFSA are very easy, and a simple example will be described herein. 

The steps required to use a given executable in VFSA are described and demonstrated in this section.

A more complex example, illustrating some of the sorts of practical real-world constraints taht a user may encounter
will be described in the final section.

There are three steps the user with a new Component must achieve:

1) Identify the files needed to run the Component program(s)

2) Create three Wrapper executables to 

a. set up a run (create a new context)

b. run (invoke the executable in that context)

c. back up a run (save the files systematically)

3) Specify an Ensemble or Multi-Ensemble

If a MECS Component already exists as a contributed module, you will only need to worry about the third part.

III.1 JSON, and Specifying the files needed to execute in context
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This presumes that your executable exists, and can read a single file for its parameters. 

First, create a directory with an appropriate name; the name must start with an uppercase letter.

Then, create an example parameter file in that directory, change your current directory to that directory. You may move any other files you wish into that directory. Having the target executable in that directory is typical, though not required. 

(Note anything else you need to do to make the executable runnable in that context; e.g., manually creating subdirectories. If there is anything of the sort, you will need to specify it in the setup script described below.)

Ensure that your target executable runs in that context and leaves all its output files in the current directory.

Now you need to tell VFSA which files need to be moved into a new context. These are all the files that you had in the directory at
the time you started the run. 

A special file or files in the Component directory called "copy*json"
should be created to contain the information MECS needs to set up the
Generation working directory. That file contains, in JSON format, a
list of files to copy or link, by default from the Component
directory.

JSON format is similar to Python dictionary source code. However, note that
trailing commas in lists, unlike in python.

In cases of interest to users of VFSA, all that you need to know about JSON is

1) The file begins with "{" and ends with "}"
2) Each subsequent line associates a name with a value or a list of values. Each association is a line containing a name, a colon, and a value or list of values
3) A name or a value are delimited before and after by a double quote (")
4) A list of values is delimited before and after by square brackets ([ ... ]) and the values are comma-separated

See the examples below.

A simple example of a copy specification is: 

::

 $ more copy.json
 {"files": ["ezparab.py"]}

This will tell MECS to copy the file "ezparab.py" into the Generation's current
working directory when the directory is set up.

Several files can be copied as follows

::

 $ more copy.json 
 {
  "files": ["runfile","datafile1","datafile2"]
 }


If the list of files is for symbolic linking only (useful when the
files are large) the copy command may be overridden with "ln"

::

 $ more copy2.json 
 {"op": "ln -s",
  "files": ["datafile3"]
 }

Notice that in the above the specification is called copy2.json ; the previous
two filesnamed copy.json and copy2.json,  may both be present. All files
matching "copy*.json" will be processed.

Additionally, the destination may be specified

::

 $ more copy.json 
 {"dest": "%%/subdir_name"
  "files": ["subdir_file1","subdir_file2","subdir_file3"]
 }

Normally files will be copied into a subdirectory of the working
directory. MECS will replace "%%" with the actual working directory.


Example
"""""""

In the MECS distribution is a subdirectory $MECSDIR/Components containing all Components developed to date.

A simple example is EZParabOrig. The code is shown here but is of no great importance. 

::

   #!/usr/bin/env python

   from random import seed, gauss
   from os import getcwd

   try:
       gen = int(getcwd().split(sep)[-1].split(".")[-1])
   except:
       print 'defaulting to zero'
       gen = 0

   with file("params.dat") as parms:
      for line in parms.readlines():
          exec( "%s = %s" % tuple(line.strip().split()) )

   seed(gen)

   noise = gauss(0,sigma)
   result = noise + (xx - atarg) ** 2 + (yy - btarg) ** 2

   with file("result.dat","w") as resfile:
      resfile.write(str(result)+"\n")

This calculates a simple parabolic function with noise. The calculation is
based on parameters and a random number seed in a file params.dat which looks like this:

::

 sigma	0.1
 atarg	0.4
 btarg	0.4
 yy	0.3
 xx	0.33

The main point in displaying this is that the code is such that it may exist in complete isolation from MECS or VFSA 
and may have been designed by somebody with no 
The constraint is that all parameters are read from a file.

It runs fine inside the Components/EZParab directory, creating a file called result.dat on exit.

Now to run this in a new directory, suppose we decide to have the following

1) a parameter file
2) the executable 

The parameter file is a special case. So the only file that needs copying is the python source.

So we create a file called "copy.json" with contents

::

   {"files": ["ezparab.py"]}


NOTE: Coming soon - similar functionality for backing up after the run. These specification files
will match "save*.json".


III.2) Specifying the Component methods
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

VFSA requires two Components. One component is the Model component, and the other is the Cost component.

If you have code that you want to run in VFSA, you must make it into a Component. This is not difficult.

A Component must be specified in a directory. The directory name must
start with an uppercase letter.

You must write some extension code in the language of your choice. In
many cases this code is very simple, but it can be as complex as suits
your purposes and the requirements of the Components.

VFSA requires the methods "setup", "run", and
"backup" for  each Component. These must be executables, and may alternatively
be called 

1) "setup.sh" or "setup.py", or simply "setup" and similarly 
2) "run", "run.sh" or "run.py"
3) and "backup", "backup.sh" or "backup.py" 

Shell scripts, executables and python scripts are handled slightly
differently, so be sure to use the appropriate extension. Ensure that you as user have
execute priviliges on these files.

It is possible that you may not wish to have any actions occuring in a
given step. This is okay. You may create an empty executable called
"setup.sh" or "setup.py". The easiest way to do this is with the
"touch" command:

::

 touch setup.py

The setup script must perform any preliminaries (if any) needed to run the
Component. 

The "run" script must invoke the Component.  The "run" script should
return 0 on success, or some nonzero status on irretrievable failure

The "backup" script may make copies of results to nonvolatile backup
media. Note that often on high performance media, files are temporary
by default, and any results which are needed for future analysis
should be saved to permanent media.

In principle, ".py" is preferred for very small components, as the
performance will be very fast. For most envisioned applications this doesn't matter.

Example
"""""""

In EZParab, the setup.py and backup.py files are empty. The run.py simply invokes the
shell and verifies the existence of the output

::

   #!/usr/bin/env python
   from os import system
   from os.path import isfile

   system("python ezparab.py")
   assert isfile("result.dat")


III.3) Specifying the Ensemble
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two parts to specifying a VFSA ensemble:

1) Create the Ensemble Specification
2) Create the parameter template file
3) Create the parameter range file

III.3.1) Create the Ensemble Specification
""""""""""""""""""""""""""""""""""""""""""

In order to specify an Ensemble, the user must first create a directory,
with an arbitrary name beginning with an uppercase letter.

Suppose you call it *MyEnsemble* . You may create this anywhere you like
on nonvolatile media. (It should NOT be on temporary storage, "scratch"
disks, etc.)

Within the ensemble specification directory you must create a JSON file. Currently it must have
the same name as the enclosing directory, except that all uppercase letters
are changed to lowercasei, and have extension ".jspec". So in the case of *MyEnsemble*, it
would be called *myensemble.jspec*

Here are the values that VFSA requires for you to specify

 - expname : [any text (Currently not used and will be removed)]
 - scheme : "VFSA"
 - Model : [name of your model component]
 - Cost : [name of your cost component
 - rundir : [path to outputs]
 - MECS_MOD : [relative path to your component from mecs install, or absolute path to your component]

For example:

::

 {
 "expname" : "trybsi",
 "scheme" : "VFSA",
 "rundir" : "UnitTests/Results/Test9",
 "Model" : "EZParabOrig",
 "Cost" : "ParabCost",
 "MECS_MOD" : "Components"
 }

Optional values

 - Maxgen : [maximum number of generations to run (defaults to 5)]
 - parmfnam : [parameter file name (defaults to "parms.dat")]

You must determine reasonable values for anything listed above in square brackets.

The VFSA Scheme uses values in a parameter file based on the outcome of previous runs.

The structure of the parameter file and the bounds for each parameter are specified
in two files in a subdirectory of the Ensemble directory which you must create, called
<Path-to-Ensemple>/Vars

III.3.2) Create the Parameter Template File
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first file in Vars is called template. This is a copy of a working parameter file.
However, all the values which change over the course of the VFSA instance are replaced
in the file with tags in the form %(*variable_name*)s.

For instance, if one is changing a parameter called heat_capacity, instead of, say

::

 heat_capacity = 1.3

one would put 

::

 heat_capacity = %(heat_capacity)s

The trailing "s" is currently necessary

VFSA will use this information to construct many versions of the parameter file, each in their respective
context.

The exact format of the parameter file is of no concern, provided it does not need to contain the "%" character. 

So it could equally be

::

  "heat_capacity" : 1.3

for which one would substitute, in the template

::

  "heat_capacity" : %(heat_capacity)s


If the final parameter file does need "%" characters, they should be escaped in the template with a preceding backslash 
as "\%" .

III.3.3) Create the Parameter Bounds File
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

for each varying parameter, a range must be specified, in a file called bounds, which is in JSON format

VFSA assumes the bounds are floating point numbers, and they should be related to the name of the variable as a two-item list,
lower bound first, as in

 "heat_capacity" : [1.0,2.5]

Example
"""""""

Create a working directory MyEnsemble

Within it create a file called myensemble.jspec, which is 

The working example of EZParab used a parameter file like this:

::

 sigma	0.1
 atarg	0.4
 btarg	0.4
 yy	0.3
 xx	0.33

Suppose we want to vary xx and yy. (See Ensemble9 in the UnitTests directory)

Then we create a template file that looks like this:

::

 sigma   0.1
 atarg   0.4
 btarg   0.4
 yy     %(yy)s
 xx     %(xx)s

and a bounds file that looks like this:

::

 {
 "xx" : [0,1],
 "yy" : [0,1]
 }

You are now ready to create a running instance. At the OS prompt enter

::

 <path-to-Mecs>/mecs -s <path-to-ensemble>/<ensemble-name>

III.4 Summary of Example
------------------------

You created or inherited one directory called a Component directory. Specifically

*EZParab*

which contained

 - ezparab (executable)
 - parms.dat (sample parameter file)
 - copy.json (list of files to copy into new context)
 - run (script to run the executable)
 - setup (possibly empty script to set up the executable - usually for simple executables this is empty)
 - backup (script to process and save output files on success)

*MyEnsemble*

which contained
 - myensemble.jspec (experiment specification)
 - Vars (subdirectory) which contained
    ~ template (parameter file with substitution fields specified)
    ~ bounds (range limits for varying parameters)

Now you can invoke

::

 [path-to-mecs]mecs -s [path-to-MyEnsemble]MyEnsemble


IV Running on a Supercomputer
-----------------------------

Normally, you submit a job and the conmsole returns quickly. 

Mecs must wait until completion of a job to proceed to its next step.

Inquire of your supercomputing support team how to have your session wait for completion of the submitted job.

At TACC, this is achieved by the "-sync y" option to qsub

Here is the qsubscript I use for CAM:

::

 #$ -V
 #$ -cwd
 #$ -j y
 #$ -A A-ig2 
 #$ -l h_rt=00:30:00
 #$ -q development
 #$ -N testCAM3
 #$ -o ./$JOB_NAME.out
 #$ -sync y
 #$ -pe 12way 36
 export MY_NSLOTS=32
 ibrun ./cam

Note that this implies that the run script must submit the job to the queue, 

The minimal run script looks like this:

::

 from sys import path

 path.append(**INSERT PATH TO MECS**)
 from Util import __

 __("qsub < qsubscript",duration=360000,increment=3600)

The __ utility offers a way to submit jobs to the operating system without using resources. Duration gives a maximum number of seconds before timeout, and should exceed your allocation plus your expected queue time. Increment is a maximum number of seconds Mecs should wait before checking for output. (Early in the submission it checks more often)>

COMING SOON: A wrapper to call such a script from other languages.

IV.1 Starting and Restarting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Because MECS is intended for long-running Ensembles of long-running jobs, facility of
starting and restarting is essential. 

There are two cases which need to be accounted for. One is that MECS
itself stops running, possibly because of an outage on the platform on
which it runs. 

Ensure that none of the components are running. Then issue the same mecs
command as originally, without the "-s".


IV.1.1 Designing your Component for Restart
"""""""""""""""""""""""""""""""""""""""""""

Another possibility is that MECS continues running, but a Component
fails. Alternatively, perhaps a Component does not run to completion
because supercomputing resources were inadequate for that purpose. 

For example, CAM runs on a maximum of 64 CPUs, and at one time we had
a suite of computations that took 60 hours x 64 CPUs to run, but we
were only allocated 24 hour slices. It was necessary to start or restart each
Generation's computation three times even under normal operations.

Coping with this is necessarily component-specific and to some extent
platform-specific as well. The "run" method is designed to run the
Component to completion or return a failure status if that is
impossible for some reason. 

"Ordinary" resubmissions, and detection of whether the run has reached 
completion, is a Component-specific task. The "run" method of the
Component is expected to return 0 on success, or some nonzero status 
on irretrievable failure. 

If no account is taken for this contingency, a Component will simply
rerun in the present context. For many codes, this amounts to 
a rerun "from the beginning". For relatively small computations in
relatively stable environments this is perfectly fine.

IV.1.2 Some Caveats about Restarting
""""""""""""""""""""""""""""""""""""

Some models are designed to "pick up where they left off". (In our
particular case, we have used CAM3 which is designed to do that, but
which has a small bug on platforms other than NCAR machines. So the
restart script has to make a minor modification to a read-write file 
which points to the latest restart file.)

There are situations that the designer of the "run" method may wish to
account for. If, for example, data needed for restart is corrupted,
there is a danger of an expensive infinite loop, resubmitting the same
job and failing. So it is safest if restarts are contemplated to
detect whether progress was made since the previous submission. Again,
doing this is necessarily a model-specific task. For CAM3, we look for
the latest monthly output data file and save its name. If the job
exits the queue twice in a row with the same last file, we consider
the process "stuck", and leave the "run" routine with an error condition.

It is best to start simple and work your way up. Currently, the CAM script
(not entirely tested) looks like this:

::

 #!/usr/bin/env python

 from sys import exit
 from sys import path
 from os.path import dirname,abspath,isfile
 from os import environ,sep
 from glob import glob

 from os import getcwd

 path.append(environ.get("MECSDIR",dirname(abspath(__file__))))
 from Util import __,jload
 runname = state.get("$runname","camrun.bsi")
 lastmo = state.get("$lastmo","0019-12")
 pattern = state.get("$pattern","*.cam2.h0.*")

 debug = True

 def Print(arg,level=None):
    if debug:
        print arg

 completed = None

 # figure out which output file, if any, exists
 # in case this is a restart

 if isfile("progress"):
   with file("progress") as progress:
      try:
          lastfile = progress.readlines()[-1].strip() 
      except IndexError:
          Print("empty extant progress file","!")
          lastfile = None
 else:
   o = __("touch progress")
   lastfile = ""

 done = False
 stuck = False

 # keep submitting until done 

 while not done and not stuck:

    #fixcambug() NOT RELEVANT, OMITTED

    __("qsub < qsubscript",duration=360000,increment=3600)
    done = isfile("%s.cam2.h0.%s.nc"%(runname,lastmo))
    completed = sorted(glob(pattern))[-1]

    Print(">"+lastfile+"<","+")
    Print(">"+completed+"<","+")

    if completed == lastfile:
        stuck = True
    else:
        lastfile = completed
        with file("progress","a") as progress:
            progress.write(completed + "\n")

 if stuck:
    Print("Could not complete run.","!")
    exit(1)

 Print("run.py normal exit","+")



IV.2 How to Fail
~~~~~~~~~~~~~~~~

Your run code should return a nonzero status on failure, and a zero if it succeeds. If you write your run script in python you can achieve this
with an uncaught exception. 

On failure, the VFSA instance shuts down for user intervention.

On success, it proceeds to the backup phase and on to the next generation.

V MULTI-ENSEMBLES
-----------------

A near-future expansion of MECS is Ensembles of Ensembles. A crude
version of this facility is now provided with the mmecs command.

Again we use a directory to encapsulate a MECS concept. In this case
we have an Ensemble Specification Directory.

Within this directory is a JSON-format file called "mspec", where some
of the target strings are python template format. So the file may look
like this:

::

 {
 "toprun" : "TestMM2",
 "expname" : "%(expname)s",
 "scheme" : "VFSA",
 "maxgen" : "20",
 "rundir" : "%(targ)s",
 "Model" : "Parab",
 "MECS_MOD" : "Models"
 }

Here "toprun" is a directory in which to run all the member 
Ensembles. The forms of expname and rundir should be copied
verbatim. The other parameters are those which are the same for all
components. 

This version is designed to work with lists. If there is a List
subdirectory in the Multi-Ensemble specification, it will be divided among
the component Ensembles. 

See the UnitTests/MultiEnsemble1 directory for an example.


VI SUMMARY
----------

An Ensemble links a Scheme to one or more Components. An Ensemble
is specified with an Ensemble Specification file which is inside an
Ensemble Specification directory. A Scheme may require additional
information in this directory in a Scheme-specific way. VFSA is 
such a Scheme and it expects an Ensemble subdirectory called List.

A Component is a computation specified by the user. Little knowledge
of MECS and no Python is required to specify a Component. The Component
designer must provide scripts to set-up, run and back-up each such computation.

No knowledge of any particular language is assumed. Examples are 
in Python for the convenience of this document's author.

VII LICENSE
-----------

VFSA and MECS are Copyright (c) 2012 Board of Regents of the University of Texas

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the University of Texas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
