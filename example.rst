A REAL-WORLD EXAMPLE OF MECS USE
================================

We have done a large number of experiments with the CAM3 model in
predecessor versions of MECS. This document is intended as a
description of the use of a realistic model in the MECS framework
on the Lonestar platform at the Texas Advanced Computing Center
(TACC).

The experiences described here are not necessarily exactly those you
will encounter, but are indicative of the sorts of things you might
expect.


I Getting the Model
-------------------

You should obtain the model and its initial condition files from NCAR.

You can find links to the files on the web at 

::

  http://www.cesm.ucar.edu/models/atm-cam/download/

You do not want to download to your desktop; you can save a step
with wget. Right-click or control-click on the link you want and
select "copy link location". Then in a terminal session type "wget "
and paste the URL you saved. e.g.

::

 wget http://www.cesm.ucar.edu/models/atm-cam/download/cam3.1/cam3.1.p2_source_code.tar.gz

This will copy the "tarball" archive to the target machine. On TACC
you should have a working directory on the $WORK drive for this. $WORK
is nonvolatile storage with enough user space for these files.

You will need the source distribution and the initial conditions. We
use T42 resolution:

::

 wget http://www.cesm.ucar.edu/models/atm-cam/download/cam3.1/cam3.1_64x128_T42_datasets.tar.gz

Then expand these with

::

 tar -xzvf *gz

(After some hard knocks, we determined that CAM reads its namelist via
stdin (the unix "standard input") and the TACC configuration does not
allow this. Therefore we had to apply the following patch:)

::


 diff -ru cam1/models/atm/cam/src/control/runtime_opts.F90 ../CAM3/cam1/models/atm/cam/src/control/runtime_opts.F90
 --- cam1/models/atm/cam/src/control/runtime_opts.F90 2005-03-08 11:06:36.000000000 -0600
 +++ ../CAM3/cam1/models/atm/cam/src/control/runtime_opts.F90 2012-02-15 17:57:05.000000000 -0600
 @@ -1065,7 +1065,15 @@
 !
 ! Read in the camexp namelist from standard input
 !
 - read (5,camexp,iostat=ierr)
 +! read (5,camexp,iostat=ierr)
 +
 +
 + open(16,file='namelist',iostat = ierr)
 + write(6,*) 'READING NAMELIST'
 + read(16, camexp)
 + write(6,*) 'FINISHED READING NAMELIST'
 +
 +
 if (ierr /= 0) then
 write(6,*)'READ_NAMELIST: Namelist read returns ',ierr
 call endrun
 diff -ru cam1/models/lnd/clm2/src/main/controlMod.F90 ../CAM3/cam1/models/lnd/clm2/src/main/controlMod.F90
 --- cam1/models/lnd/clm2/src/main/controlMod.F90 2005-04-08 13:27:10.000000000 -0500
 +++ ../CAM3/cam1/models/lnd/clm2/src/main/controlMod.F90 2012-02-16 15:26:58.000000000 -0600
 @@ -369,7 +369,14 @@
 fsurdat=lsmsurffile
 finidat=lsminifile
 #else
 - read(5, clmexp, iostat=ierr)
 + ! read (5,camexp,iostat=ierr)
 +
 +
 + ! open(16,file='namelist',iostat = ierr)
 + write(6,*) 'READING NAMELIST 2'
 + read(16, clmexp, iostat = ierr)
 + write(6,*) 'FINISHED READING NAMELIST 2'
 +
 if (ierr /= 0) then
 if (masterproc) then
 write(6,*)'error: namelist input resulted in error code ',ierr

Go to the directory above cam1 where you unpacked the source and issue

::

 patch -p0 < patchCAM

We did a restart run, so we ended up with this namelist

::

 &camexp
  absems_data		= '/work/00671/tobis/CAM3Data/atm/cam/rad/abs_ems_factors_fastvx.c030508.nc'
  aeroptics		= '/work/00671/tobis/CAM3Data/atm/cam/rad/AerosolOptics_c040105.nc'
  bnd_topo		= '/work/00671/tobis/CAM3Data/atm/cam/topo/topo-from-cami_0000-09-01_64x128_L26_c030918.nc'
  bndtvaer		= '/work/00671/tobis/CAM3Data/atm/cam/rad/AerosolMass_V_64x128_clim_c031022.nc'
  bndtvo		= '/work/00671/tobis/CAM3Data/atm/cam/ozone/pcmdio3.r8.64x1_L60_clim_c970515.nc'
  bndtvs		= '/work/00671/tobis/CAM3Data/atm/cam/sst/sst_HadOIBl_bc_64x128_clim_c020411.nc'
  caseid		= 'camrun.bsi'
  iyear_ad		= 1950
  mss_irt		= 0
  rest_pfile		= './cam2.camrun.rpointer'
  nestep		= -7335
  nsrest		= 1 
 /
 &clmexp
  rpntpath		= './lnd.camrun.rpointer'
  fpftcon		= '/work/00671/tobis/CAM3Data/lnd/clm2/pftdata/pft-physiology'
  fsurdat		= '/work/00671/tobis/CAM3Data/lnd/clm2/srfdata/cam/clms_64x128_USGS_c030605.nc'
 /

The strings refer to files; and the ".rpointer" files are local files
containing strings referring to where "restart" files are located.

II Building Your Model
----------------------

A build on TACC currently looks like this:

::

 unsetenv USER_FC
 module load netcdf
 setenv INC_NETCDF /opt/apps/pgi7_2/intel11_1/4.1.1/include
 setenv LIB_NETCDF /opt/apps/pgi7_2/intel11_1/4.1.1/lib/
 setenv INC_MPI /opt/apps/intel11_1/mvapich/1.6/include
 setenv LIB_MPI /opt/apps/intel11_1/mvapich/1.6/lib
 mkdir buildpar
 cd buildpar
 ../cam1/models/atm/cam/bld/configure -spmd

then edit the Makefile, line 190, replacing "$(FC)" with "mpif90" .

then type

::
 
 make

III Submitting Your Model
-------------------------

On TACC, the submit script looks like this:

::

 #$ -V
 #$ -cwd
 #$ -j y
 #$ -A <ACCOUNT> 
 #$ -l h_rt=12:30:00
 #$ -q normal
 #$ -N testCAM3
 #$ -o ./$JOB_NAME.out
 #$ -pe 12way 72
 export MY_NSLOTS=64
 ibrun ./cam

If that is saved into a file, named, say "qsubscript" it is now possible to
invoke the model with

::

 qsub < qsubscript

note: substitute your own account for <ACCOUNT>

In practice, there may be considerable iteration among the various
steps above. Do not expect things to work "out of the box" like
commercial software. Administrative staff at the supercomputer center
tries to be helpful. Quality of assistance may vary. Persist.

All of this is the normal tedium of supercomputing. Once you get past
this point, MECS will make matters clear sailing.

IV Modifying Your Model for Parameter Variation
-----------------------------------------------

MECS presumes that all of your varying run parameters are in a
file.

Whether these are run parameters that the Component authors make available
to you or not depends on their interests and upon your own. 

If not, you will have to modify your code to achieve this. Note
that in a parallel computation this may not be entirely trivial.

If there is already a parameter file, you may add your extra
parameters to it, or make a separate file for MECS to use. If for some
reason your Component needs multiple input files, you will need to
write some code for the MECS setup phase. (More on this below.)

In the case of CAM, we decided to add the extra parameters to the
existing oarameter file, which in keeping with Fortran convention is
called namelist. The working namelist file above needed to be
expanded. So there needed to be changes in the namelist declarations
and the broadcasting of the quantities among machines.

Here for example is the set of changes required to add six
parameters:

::

 diff -u -r -N cam3/cam1/models/atm/cam/src/control/my_params.F90 cam3_mod2/cam1/models/atm/cam/src/control/my_params.F90
 --- cam3/cam1/models/atm/cam/src/control/my_params.F90	1969-12-31 18:00:00.000000000 -0600
 +++ cam3_mod2/cam1/models/atm/cam/src/control/my_params.F90	2005-10-15 07:35:00.000000000 -0500
 @@ -0,0 +1,19 @@
 +module my_params
 +!
 +!	Data block for user defined physics parameters
 +!
 +   use shr_kind_mod, only: r8 => shr_kind_r8
 +
 +   implicit none
 +   private
 +   save
 +
 +   real(r8), public ::&
 +	my_tau		=-1.,	&! setting parameters negative causes to use 
 +	my_rhminl	=-1.,	&! default values in the model
 +	my_rhminh	=-1.,	&
 +	my_ke		=-1.,	&
 +	my_c0		=-1.,	&
 +	my_alfa		=-1. 
 +
 +end module my_params

 diff -u -r -N cam3/cam1/models/atm/cam/src/control/runtime_opts.F90 cam3_mod2/cam1/models/atm/cam/src/control/runtime_opts.F90
 --- cam3/cam1/models/atm/cam/src/control/runtime_opts.F90	2004-09-20 15:07:36.000000000 -0500
 +++ cam3_mod2/cam1/models/atm/cam/src/control/runtime_opts.F90	2005-10-15 06:52:49.000000000 -0500
 @@ -57,6 +57,7 @@
     use abortutils, only: endrun
     use ramp_scon, only: bndtvscon
     use ghg_surfvals, only: bndtvghg
 +   use my_params
  
  !-----------------------------------------------------------------------
  !- module boilerplate --------------------------------------------------
 @@ -731,7 +732,8 @@
                      aero_carbon, aero_feedback_carbon, &
                      aero_sea_salt, aero_feedback_sea_salt, &
                      brnch_retain_casename, bndtvscon, bndtvghg, &
 -                    ramp_co2_start_ymd, ramp_co2_annual_rate, ramp_co2_cap
 +                    ramp_co2_start_ymd, ramp_co2_annual_rate, ramp_co2_cap, &
 +	 	    my_alfa, my_tau, my_ke, my_c0, my_rhminl, my_rhminh
  
  #endif
  
 @@ -1563,6 +1565,16 @@
     call mpibcast (aero_sea_salt,         1 ,mpilog, 0,mpicom)
     call mpibcast (aero_feedback_sea_salt,1 ,mpilog, 0,mpicom)
  
 +!
 +! Custom model physics parameters
 +!
 +   call mpibcast (my_alfa,           1 ,mpir8, 0,mpicom)
 +   call mpibcast (my_tau,           1 ,mpir8, 0,mpicom)
 +   call mpibcast (my_ke,           1 ,mpir8, 0,mpicom)
 +   call mpibcast (my_c0,           1 ,mpir8, 0,mpicom)
 +   call mpibcast (my_rhminl,           1 ,mpir8, 0,mpicom)
 +   call mpibcast (my_rhminh,           1 ,mpir8, 0,mpicom)
 +
     return
  end subroutine distnl
  #endif
 diff -u -r -N cam3/cam1/models/atm/cam/src/control/wrap_mpi.F90 cam3_mod2/cam1/models/atm/cam/src/control/wrap_mpi.F90
 --- cam3/cam1/models/atm/cam/src/control/wrap_mpi.F90	2004-03-30 16:10:57.000000000 -0600
 +++ cam3_mod2/cam1/models/atm/cam/src/control/wrap_mpi.F90	2005-10-15 07:03:34.000000000 -0500
 @@ -41,6 +41,32 @@
     end subroutine mpibarrier
   
  !****************************************************************
 +
 +   subroutine mpirank (comm, rank)
 +
 +   use shr_kind_mod, only: r8 => shr_kind_r8
 +   use mpishorthand
 +   use abortutils, only: endrun
 +
 +   implicit none
 +!
 +! Return current cpu number
 +!
 +   integer, intent(in):: comm
 +   integer, intent(out):: rank
 + 
 +   integer ier   !MP error code
 + 
 +   call mpi_comm_rank (comm, rank, ier)
 +   if (ier.ne.mpi_success) then
 +      write(6,*)'mpi_rank failed ier=',ier
 +      call endrun
 +   end if
 + 
 +   return
 +   end subroutine mpirank 
 + 
 +!****************************************************************
   
 diff -u -r -N cam3/cam1/models/atm/cam/src/physics/cam1/zm_conv.F90 cam3_mod2/cam1/models/atm/cam/src/physics/cam1/zm_conv.F90
 --- cam3/cam1/models/atm/cam/src/physics/cam1/zm_conv.F90	2004-05-10 16:39:47.000000000 -0500
 +++ cam3_mod2/cam1/models/atm/cam/src/physics/cam1/zm_conv.F90	2005-10-15 08:48:50.000000000 -0500
 @@ -53,6 +53,8 @@
    use dycore,       only: dycore_is, get_resolution
    use pmgrid,       only: masterproc
  
 +  use my_params

 +   if ( my_tau .gt. 0. ) then
 +	tau = my_tau
 +   endif
 +   if ( my_ke .gt. 0. ) then
 +	ke = my_ke
 +   endif
 +   if ( my_c0 .gt. 0. ) then
 +	c0 = my_c0
 +   endif
 
Such changes mostly replace the existing values with values from the
new namelist.

Now rebuild with these changes and make sure that it still works, and
that setting the new parameters (e.g., my_c0) changes the output.
 
V Building a Minimal MECS Component
-----------------------------------

Now that you have code which reads the values you want to vary, you
are ready to insert it into MECS. This should be much simpler than
setting up the model in the first place!

First, create a Component-specific directory - this will become your
component. Typically it is named after your Component. So we create a
directory $MECSDIR/Components/CAM

Think of this as a prototype for the directory in which your Component
will run. You can put any files you want in there - documentation,
build scripts, etc. Put the executable, the submission script, and any
other files you will need. Put a sample parameter file if you like,
for reference purposes, though the main purpose of MECS is to create a
new context with a new parameter file.

Create an empty file called "setup.py" and another empty file called
"backup.py", and create a file called "run.py" which invokes your
queue submission:

::

 from os import environ
 from os.path import abspath,dirname
 from sys import path
 
 path.append(environ.get("MECSDIR",dirname(abspath(__file__))+"../.."))
 from Util import __
 from Util import jload
 
 state = jload(file("state.json"))
 
 if state.get("Production",False):
     __("qsub < qsubprod",duration=86400,maxincrement=1200)
 else:
     __("qsub < qsubscript",duration=3600,maxincrement=120)
 

This is a rather complex run script - it reads the MECS state, and
chooses between two qsubscripts depending on whether "Production" is
in the MECS state description. It uses the __() double-underscore
utility to run the shell - this will inform MECS on failure and
terminate MECS. Note that this determines whether a 24 hour or a 1
hour timeout is used.

You could get away with the much simpler "run.sh":

::
  
  qsub < qsubscript

If you use the timeout value in MECS's qsub utility, you should allow
more time than the actual run duration of your job: time waiting in
the queue also counts toward the timeout.

VI Modifying the Submission Script
----------------------------------

THIS IS IMPORTANT!

When running MECS on the head node of a supercomputer, it is necessary
to run the submission in a way that the calling process "sleeps" until
the job finishes.

If run interactively, the qsub command will appear to "hang" until the
job completes. This is what we want! (There are alternative approaches
but they have the disadvantage of being system-specific).

On TACC, the line 

::

 #$ -sync y

must be added to the qsubscript, e.g.:

::
 
 #$ -V
 #$ -cwd
 #$ -j y
 #$ -A <ACCOUNT> 
 #$ -l h_rt=00:10:00
 #$ -q development
 #$ -N testCAM3
 #$ -o ./$JOB_NAME.out
 #$ -sync y
 #$ -pe 12way 36
 export MY_NSLOTS=32
 ibrun ./cam
 
VII Identifying the Files to Copy
---------------------------------

The files which are necessary in the run context must be listed in a
JSON format file as a list of strings associated with the name
"files", in a file called copy*.json, where the * is any character
string or none. Specifically in the example directory we have copy.json:

::

 {"files": ["qsubscript","cam", "cam2.camrun.rpointer", "lnd.camrun.rpointer", "qsubprod"]}

Larger files which are not conveniently kept in the Components
directory are copied using a separate specification, where the origin
of the files is specified with keyword "src". So copy1.json may look
like:

::

 {
 "src" : "/work/00671/tobis/CAM_Component/CAM/",
 "files" : ["camrun.bsi.cam2.h0.0020-12.nc", "camrun.bsi.cam2.r.0021-01-01-00000","camrun.bsi.clm2.h0.0020-12.nc","camrun.bsi.clm2.r.0021-01-01-00000"]
 }

VIII A Dummy Cost Function
--------------------------

You may wish to set up a dummy Cost Function and go directly to the
VFSA scheme.

You can have an empty
setup and backup script, but the VFSA logic requires that the Cost
component create a text file called result.dat whose last line is a
numerical "cost". We can just put a dummy calculation in place, so
that run.py looks like:

::

 with file("result.dat") as result: result.write("1.0\n")

which will give a cost of 1.0 every time.

This is in $MECSDIR/Components/CAMCostDummy

IX An Ensemble Specification
----------------------------

So now we can create an Ensemble specification:

We create a directory CX0 (in the distribution it is in 
$MECSDIR/UnitTests/CAMExps/ ) and in it create the file cx0.jspec ,
which specifies the way MECS will run the ensemble.

Note that the name of the experiment matches the name of the jspec
file, with the latter all lower case, and also matches the "expname"
parameter.

::

 {
 "expname" : "cx0",
 "scheme" : "VFSA",
 "rundir" : "/scratch/00671/tobis/CX0",
 "Model" : "CAM",
 "Cost" : "CAMCostDummy",
 "MECS_MOD" : "../../Components",
 "Maxgen" : 2
 }

The scheme is the overall stratgey taken by MECS, here VFSA

The rundir specifies where the context will be built and (as we have
set it up in this case) where the output will fo.

The Model is the Component that wraps CAM and the Cost is the dummy
Component.

MECS_MOD is a path to the Components directory. (At present it is
assumed that all Components are in the same directory)

Maxgen specifies the maximum number of iterations of MECS. Since this
is only a test, two will suffice.

X Specifying the Variables
--------------------------

As a VFSA Ensemble, CX0 requires a subdirectory which must be called "Vars/" to 
inform VFSA of the parameter set and its bounds.

This is done with two files: "template" and "bounds"

"template" is very much like the ultimate desired parameter files;
however the values are replaced with %(NAME)s where NAME is the name
of the parameter. Ordinarily the name is the same in MECS as it is in
the Model Component, so you get redundant-looking file like this:

::

 my_alfa = %(my_alfa)s
 my_tau	= %(my_tau)s
 my_ke	= %(my_ke)s
 my_c0	= %(my_c0)s

accompanying the "template" file is a "bounds" file with the ranges of
the parameters specified in a JSON format:

::

 {
 "my_alfa" : [0.05,0.6],
 "my_tau" : [1800.0,28800.0],
 "my_ke" : [0.5E-6,10.0E-6],
 "my_c0" : [0.25E-3,6.0E-3]
 }

Now you are ready to run. Just invoke

::

 <path-to-mecs>/mecs -s <path-to-ensemble-spec>

which can be as simple as 

::

 mecs -s CX0

if mecs is in your path and CX0 is in your working directory; this is
the usual way it is formed

You should eventually see two output subdirectories in your output
directory called g.0000 and g.0001

XI Creating a Real Cost Function
--------------------------------

The actual cost function is outside the scope of this document. We are
currently using the domain-specific language Ferret to do the cost
computation, with a minor assist from python at the end, appending a
final overall cost to the result.dat file which contains numerous
component costs. 

Designing and testing a cost function in practice may be demanding and
careful testing is suggested.

In the present case 

XII The Production Ensemble
---------------------------

We will now create a modified copy of the Ensemble Specification which
is sufficient for production. We create an Ensemble directory CX,
including cx.jspec as follows:

::

 {
 "expname" : "cx",
 "scheme" : "VFSA",
 "rundir" : "/scratch/00671/tobis/CX",
 "Model" : "CAM",
 "Cost" : "CAMCostFer",
 "MECS_MOD" : "../../Components",
 "Production" : 1
 "Maxgen" : 400
 }

In addition to the new name, we have replaced the dummy cost function
with the hard-won real one, raised the maximum generation to 400, and
set a "Production" flag, which will tell run.py to use a production 
qsub script and run in 24 hour chunks before timeout (see section V)

::

 #$ -V
 #$ -cwd
 #$ -j y
 #$ -A A-ig2 
 #$ -l h_rt=04:00:00
 #$ -q normal
 #$ -N testCAM3_2
 #$ -o ./$JOB_NAME.out
 #$ -sync y
 #$ -pe 12way 36
 export MY_NSLOTS=32
 ibrun ./cam

Finally, you should consider what you want in your backup
script. Typically your results will be on a volatile disk. You should
do whatever data compression and copying to persistent media as
necessary in that script.

XIII Robustness to Partial Computations
---------------------------------------

An important design constraint of MECS is the ability to robustly pick
up where it left off. If your Components are sufficiently granular
that you do not mind rerunning them in case of an occasional failure,
you are done. If mecs stops, you can restart it with teh same
experiment name, and it will pick up where it left off, redoing the
interrupted step.

In some cases, the interrupted step can be expensive. For example, if
the interrupted calculation involves many CPUs for many hours, it is
wasteful to throw away a partial calculation.

In the extreme case, your maximum time allocation will be shorter than
your run duration. In that case, the major computation will never run
to completion in one invocation. (Usually you can negotiate this limit
with your sysadmins but it is conceviable that this is infeasible in
some circumstances.)

If a computation is large enough that resubmission is needed for one
of these reasons, it is first necessary to establish that the code
itself supports such recovery. Most large supercomputing codes will do
so.

In this case it is a requirement of your run script to cope with the
problem.

Note, however, that the run script should also ensure that it does not
repeatedly submit jobs where no progress is made. So your run script
should also be smart enough to notice that condition and exit with an
error condition in that case.

Here is a modified run script for CAM that accounts for these issues:

::
 
 # ESSENTIALLY THE PREVIOUS RUN SCRIPT

 from os import environ,sep
 from os.path import abspath,dirname
 from sys import path, exit
 from os.path import isfile
 
 path.append(environ.get("MECSDIR",dirname(abspath(__file__))+"../.."))
 from Util import __
 from Util import jload
 
 def run(state):
     if state.get("Production",False):
         __("qsub < qsubprod",duration=86400,maxincrement=1200)
     else:
         __("qsub < qsubscript",duration=3600,maxincrement=120)


 # READ STATE INFORMATION
 
 state = jload(file("state.json"))
 runname = state.get("$runname","camrun")
 lastmo = state.get("$lastmo","0024-12")
 pattern = state.get("$pattern","*.cam2.h0.*")
 expdir = state["_expdir"]
 
 # SET UP DIAGNOSTICS

 import logging
 #logging.basicConfig(filename=expdir+sep+__file__.split(sep)[-1]+"mecs.log",level=logging.DEBUG)
 logger = logging.getLogger('CAM2 run.py')
 logger.setLevel(logging.DEBUG)
 # create file handler which logs even debug messages
 fh = logging.FileHandler(expdir+sep+abspath(__file__).split(sep)[-2]+"_mecs.log")
 #import pdb;pdb.set_trace()
 fh.setLevel(logging.DEBUG)
 # create console handler with a higher log level
 ch = logging.StreamHandler()
 ch.setLevel(logging.INFO)
 # create formatter and add it to the handlers
 formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
 ch.setFormatter(formatter)
 fh.setFormatter(formatter)
 # add the handlers to logger
 if state.get("Verbose",0):
     logger.addHandler(ch)
 logger.addHandler(fh)
 
 def Print(arg,level=None):
     if not level:
         logger.info(arg)
     else:
         if level == "!":
             logger.warn(arg)
         else:
             logger.info(arg)
 
 # WORK AROUND RPOINTER BUG WHEN RESTARTING CAM

 def fixrpointers():
     from glob import glob
     flist = glob("*rpoint*")
     for fnam in flist:
         with file(fnam) as rpfile:
             text = rpfile.readlines()
         __("rm %s"%fnam)
         with file(fnam,"w") as rpout:
             for line in text:
                 if sep in line:
                     line = "." + sep + line.split(sep)[-1]
                 rpout.write(line)
 
 # CHECK IF WE HAVE BEEN HERE BEFORE

 if isfile("progress"):
    with file("progress") as progress:
       try:
           lastfile = progress.readlines()[-1].strip()
       except IndexError:
           Print("empty extant progress file","!")
           if isfile("snagged"):
               Print("Repeated empty extant progress file","!")
               exit(2)
           else:
               o = __("touch snagged")
           lastfile = ""
 else:
    o = __("touch progress")
    lastfile = ""
 
 done = False
 stuck = False
 
 # RUN AND CHECK FOR DONE

 while not done and not stuck:
     fixrpointers()
     run(state)
     done = done or isfile("%s.cam2.h0.%s.nc"%(runname,lastmo))
     if not done: #CHECK FOR PROGRESS, COME BACK FOR MORE IF SO
         try:
             completed = sorted(glob(pattern))[-1]
         except IndexError:
             Print("no computations","!")
             o = __("touch snagged")
             exit(1)
         Print(">"+lastfile+"<","+")
         Print(">"+completed+"<","+")
         if completed == lastfile:
             stuck = True
         else:
             lastfile = completed
             with file("progress","a") as progress:
                 progress.write(completed + "\n")
     
 if stuck:
     Print("Could not complete run.","!")
     exit(1)
 
 Print("run.py normal exit","+")
 
As you can see this is rather complicated. Simplifying of logging is
in progress, but coping with the resubmission without risking an
infinite loop requires some Component-specific logic.

License
-------

Lister and MECS are Copyright (c) 2012 Board of Regents of the University of Texas

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the University of Texas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
