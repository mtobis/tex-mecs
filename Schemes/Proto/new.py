from sys import argv
from string import uppercase

assert len(argv) == 2

expname = argv[1]

assert expname[0] in uppercase

from os import environ,sep,mkdir
from os.path import dirname,isdir,abspath

mecsdir = environ.get("MECSDIR",dirname(abspath(dirname(__file__))))

specdir =  mecsdir + sep + expname


queries = {    
    "rundir" :  mecsdir + sep + "Res" + expname,
    "Model"  : "Parab"
           }

values = {"scheme" : "MetaScheme", "expname" : expname.lower(),"MECS_MOD" : "Models"
}

def defq(key):
    rawval = raw_input(key + ": ").strip()
    if not rawval:
        return None
    return rawval

    
for k,v in queries.items():
    values[k] = defq(k+" [%s]"%v) or v

    #import pdb;pdb.set_trace()

specfile = specdir + sep + specdir.split(sep)[-1].lower() + ".jspec"
if not isdir(specdir):
    mkdir(specdir)

with file(specfile,"w") as sf:
    sf.write("{")
    first = True
    for k,v in values.items():
        if first:
            first = False
        else:
            sf.write(",")
        sf.write("\n")
        sf.write(' "'+k+'" : "'+v+'"')
    sf.write('}\n')
