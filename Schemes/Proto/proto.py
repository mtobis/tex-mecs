# Proto scheme

# The Model Ensemble Control System (also known as MECS or Tex-MECS) is
# Copyright (c) 2012 Board of Regents of the University of Texas
# 
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, 
# this list of conditions and the following disclaimer.
# 
# Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation 
# and/or other materials provided with the distribution.
# 
# Neither the name of the University of Texas nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
# THE POSSIBILITY OF SUCH DAMAGE.

from os import environ, mkdir, sep, getcwd, chdir
from os import system as _
from os.path import isdir,isfile, abspath, dirname
from glob import glob
import json
import logging

from sys import path
from os import environ

if "MECSDIR" in environ:
    mecsdir = environ["MECSDIR"]
else:
    mecsdir = sep.join(abspath(__file__).split(sep)[:-1])
    environ["MECSDIR"] = mecsdir

#mecsdir = environ["MECSDIR"]

MAXGEN = 5

from ModelWrap import ModelWrap
from Util import jload, jloads

Verbose = False

class StopMECS(Exception):
    pass

"""
def Print(arg):
    logging.debug(arg)
    if Verbose: print arg
"""


from Util import Print as PPP

def Print(arg,lev=None):
    return PPP(arg,logger,lev)

def callgen(thing):
    Print ("callgen "+str(thing.sequence))
    for item in thing.sequence:
        #Print(">>"+item)
        yield item

NoContinue = False

def docopy(copyfnam,jdict,defdest):
    from glob import glob

    Print("- docopy")

    copydat = open(copyfnam)

    copydict = jload(copydat)
    copydat.close()

    source = copydict.get("src",getcwd()) + sep
    if not source.endswith(sep): 
        source += sep
    op = copydict.get("op","cp")

    if "dest" in copydict:
        dest = copydict["dest"].replace("%%",defdest)
    else:
        dest = defdest

    if not dest.endswith(sep): 
        dest += sep

    for item in copydict["files"]:
        cmd = "%s %s/%s %s" % (op,source,item,dest) #
        with open(environ["HOME"]+"/test.txt","w") as cmdout:
            cmdout.write(getcwd())
            cmdout.write(cmd)

        if _(cmd):
            print "docopy : %s failed" % cmd
            raise IOError

    compname = getcwd().split("/")[-1]
    exper_src = jdict["_exp_parms"]+"/" + compname
    if isdir(exper_src):
        flist = glob(exper_src+"/*")
        for fnam in flist:
            #fqnam = exper_src+"/"+fnam
            cmd = "cp %s %s" %(fnam,dest)
            if _(cmd):
                print "docopy : %s failed" % cmd
                raise IOError


class component(object):
    def __init__(self,compdict,jdict):
        self.compdict = compdict
        self.jdict = jdict
        name = self.compdict["ref"]
        self.name = name

    def validate(self):
        Print(". validate")
        for step in self.compdict["needs"]:
            assert self.wrap.can(step)

    def wrap(self,dir=None):
        jdict = self.jdict
        Print(". wrap")

        name = self.name
        if dir:
            self.dir = dir
        else:
            self.dir =  jdict["_mecsmod"] + sep + jdict[name]
        #Print(self.dir)
        #Print(self.jdict)


        self.wrap = ModelWrap(self.dir)

        for item in self.compdict["needs"]:
            #print self.name, item
            exec("self.%s = self.wrap.%s"%(item,item))
            #raw_input("\n:") 

    def initialize(self):
        Print(". init")
        
        cwd = getcwd()

        chdir(self.dir) 

        self.vdict = {}
        self.pdict = {}

        self.copylists = glob("copy*json")
        self.backuplists = glob("backup*json")
        chdir(cwd)
        

    def newgen(self,gendir):

        #Print (". newgen" + self.name)
        cwd = getcwd()
        chdir(self.dir)

        for copyfnam in self.copylists:
            #Print ("gd="+gendir)
            docopy(copyfnam,self.jdict,gendir)
        chdir(cwd)

                    
class scheme(object):

    def __init__(self,jdict, loghandle, schemedir = None,sequence=None):
        global logger
        logger = loghandle

        if not schemedir:
            schemedir = dirname(__file__)

        self.jdict = jdict
        assert type(jdict) == dict

        self.required_dirs = ["_expdir"] #,"Model"]
        
        if sequence is None:
            self.sequence = ["Model.run","update"]
        else:
            self.sequence = sequence

        Print("sequence = %s" %str(self.sequence))

        f = file(schemedir + sep + "scheme.json") 
        self.compspec = jload(f) 
        f.close()

        self.required_vars = ["_expname"]

        self.cc = callgen(self)
        self.gen = None
        self.chain = 0
        #import pdb; pdb.set_trace()
        #self.maxgen = jdict.get("maxgen",MAXGEN)

    def save(self,fnam):
        Print("* save")
        
        state = self.jdict.copy()
        for item in dir(self):
            if "_" not in item and "state" not in item and "jdict" not in item:
                rep = eval("repr(self.%s)" % item)
                if "object" not in rep:
                    state[item] = eval("str(self.%s)" % item) #rep
        self.state = state

        statefile = open(fnam,"w")
        json.dump(state,statefile,indent=4)
        statefile.close()
        pass

    def recover(self,fnam):
        pass

    def startup(self,chain=None,gen=0):
        for item in self.required_dirs:
            itemdir = self.jdict[item]
            assert isdir(itemdir)
            self.__dict__[item] = abspath(itemdir)

        for item in self.required_vars:
            assert item in self.jdict
        self.components = {}

        for compname in self.compspec:
            compdir = self.jdict["_mecsmod"] + sep + self.jdict[compname]
            try:
                assert isdir(compdir)
            except AssertionError:
                print "no such component directory %s" % compdir
                raise
            this = component(self.compspec[compname],self.jdict)
            this.wrap()
            this.validate()
            cwd = getcwd()
            this.initialize()
            self.components[compname] = this
            exec("self.%s = this" % compname) 
            chdir(cwd)

        jdict = self.jdict

        self.maxgen = self.jdict.get("Maxgen",MAXGEN)
        
        self.gen = self.jdict.get("gen",None)

        if not isinstance(self.gen,int) :
            self.gen = 0
            
        if self.gen >= self.maxgen:
            raise StopMECS

        self.jdict["dobackup"] = False

        if glob("backup*json"):
            self.jdict["dobackup"] = True
            if not backuproot in jdict:
                self.jdict["backuproot"] = environ["MECS_BACKUP"]
            assert isdir(self.jdict["backuproot"])
            
        jdict = self.jdict
        dest = jdict["_expdir"]
        dest = dest + "/g.%04d" % self.gen
        self.dest = dest

        if isdir(dest):
            if NoContinue:
                raise NotImplementedError,"Continuation of existing run."
            else:
                Print("continuing")
                self.continuation = True

        else:
            mkdir(dest)
            self.continuation = False
            jdict["step"] = self.step = 0
            for compname in self.compspec:

                self.components[compname].newgen(dest)

    def update(self,**kwargs):

        Print ("* update")

        # set generation

        if self.gen is None:
            self.gen = 0
        else:
            if self.jdict["dobackup"]:
                for backupfnam in self.backuplists:
                    genback = self.backuproot + sep + "b.%04d" % gen
                    docopy(backupfnam,self.jdict,genback)
            self.gen += 1
        if self.gen >= self.maxgen:
            raise StopMECS
        self.step = 0

        jdict = self.jdict

        dest = jdict["_expdir"]
        
        # create destination directory if needed

        dest = dest + "/g.%04d" % self.gen

        self.dest = dest

        if isdir(dest):
            if NoContinue:
                raise NotImplementedError,"Continuation of existing run."
            else:
                Print("continuing")
                self.continuation = True
        else:
            mkdir(dest)
            self.continuation = False

            # copy files as needed

            for compname in self.compspec:

                self.components[compname].newgen(dest)

    def __call__(self):         
        while True:
            try:
                Print("* call!")
                method = self.cc.next()
                cmd = "self.%s(context='%s')"%(method,self.dest)

                # hack - something is not quite right - this is a workaround - investigate
                
                if not hasattr(self,"step"):
                    try:
                        self.step = self.jdict["step"]
                    except KeyError:
                        self.step = self.jdict["step"] = 0
                
                #####

                if self.sequence.index(method) != self.step:
                    Print("=>skipping %s"+cmd)
                    self.step += 1
                else:
                    Print("**"+cmd)  
                    error = eval(cmd)
                    assert not error
                    self.save("%s/state.json"%self.dest)
                    self.save("%s/state.json"%dirname(self.dest))
                    self.step += 1
                    return
            except AssertionError:
                Print("command %s failed\n %s" % (cmd,error))
                #break

                raise ValueError,  "command %s failed\n %s" % (cmd,error)

            except StopIteration:
                self.step = 0
                self.cc = callgen(self)
            except StopMECS:
                break
        Print("normal exit")
        raise StopIteration

if __name__ == "__main__":
    raise NotImplementedError

