from sys import path

from os import environ
from os.path import dirname
schema = environ.get("MECS_SCHEMES",dirname(dirname(__file__)))
path.append(schema+"/Proto")

import pdb

import proto

from os import sep,environ
from os import system as _
from os.path import dirname,abspath,isdir,isfile
from math import exp

if "MECSDIR" in environ:
    mecsdir = environ["MECSDIR"]
else:
    mecsdir = sep.join(abspath(__file__).split(sep)[:-1])
    environ["MECSDIR"] = mecsdir

path.append(mecsdir)

MAXGEN = 5

from ModelWrap import ModelWrap
from Util import jload, jloads

PrintGen = True

if PrintGen:
    from sys import stdout

from random import seed, choice, random, gammavariate, uniform

def strtobool(boolstr):
    return str(boolstr).lower() == "true"

class scheme(proto.scheme):
    def __init__(self,jdict,logger,schemedir=None,sequence=None):
        if sequence is None:
            sequence = ["setup","Model.setup","Model.run","Model.backup","Cost.setup","Cost.run","Cost.backup","update"]
        if schemedir is None:
            schemedir = abspath(dirname(__file__))
        proto.scheme.__init__(self,jdict,logger,schemedir=schemedir,sequence=sequence)

    def setupall(self):
        jdict = self.jdict

        try:
            vardir = jdict["_exp_parms"] + "/Vars/"
            assert isdir(vardir)
        except AssertionError:
            print "no such directory %s" % vardir
            raise
        try:
            assert isfile(vardir+"bounds")
            assert isfile(vardir+"template")
        except:
            print "bounds and template files needed in %s" % vardir
            raise
        
        self.NTARGET = int(jdict.get("NTARGET",37))     #reconsider this section TODO
        self.TZERO = float(jdict.get("TZERO",3.5))
        self.TMIN = float(jdict.get("TMIN",1.0e-7))
        self.ALFA = float(jdict.get("ALFA",0.))
        self.BETA = float(jdict.get("BETA",1.))
        self.AA = float(jdict.get("AA",0.9))
        self.BB = float(jdict.get("BB",0.5))
        self.PARMFNAM = jdict.get("PARMFNAM","params.dat")
        self.SEED = jdict.get("SEED",0)

        self.error1 = float(jdict.get("error1",200))
        self.error2 = float(jdict.get("error2",200))
        self.errmin = float(jdict.get("errmin",200))
        self.bsistep = strtobool(jdict.get("bsistep",False))
        self.firstSample = strtobool(jdict.get("firstsample",True))
        self.bsistep = strtobool(jdict.get("bsistep",False))
        self.nfail = int(jdict.get("nfail",0))
        self.bestgen = int(jdict.get("bestgen",0))
        self.acceptgen = int(jdict.get("acceptgen",0))

        with open(vardir+"bounds") as boundfile:
            self.BOUNDS = jload(boundfile)
        with open(vardir+"template") as tplfile:
            self.TEMPLATE = tplfile.read()

        seed(self.SEED)

        self.vars = eval(jdict.get("vars","{}"))
        self.mins = eval(jdict.get("mins","{}"))
        self.maxs = eval(jdict.get("maxs","{}"))
        self.best = eval(jdict.get("best","{}"))
        self.accepted = eval(jdict.get("accepted","{}"))

        bounds = self.BOUNDS
        for key in sorted(bounds.keys()):
            varbds = bounds[key]
            assert varbds == sorted(varbds)
            if not isfile (self.dest + sep + self.PARMFNAM): # first time
                self.vars[key] = uniform(*varbds)
                self.best[key] = self.vars[key]
            self.mins[key], self.maxs[key] = varbds
            
        if not isfile (self.dest + sep + self.PARMFNAM):
            parmdata = self.TEMPLATE % self.vars
            with open(self.dest + sep + self.PARMFNAM,"w") as parmfile:
                parmfile.write(parmdata)

        if abs(self.ALFA) < 1.0e-6:
            self.Sval = self.BETA
        else:
            self.Sval = gammavariate(self.ALFA,1./(self.error2 + self.BETA))
 
    def startup(self):
        proto.scheme.startup(self)
        self.setupall()
            
    def update(self,**kwargs):

        S = self

        with open(S.jdict.get("resultfile","result.dat")) as rf:
            S.error2 = abs(float(rf.readlines()[-1].strip()))

        if(S.firstSample):
           S.firstSample=False
           S.error1 = S.error2
           S.errmin = S.error2
           for key in S.vars.keys():
                S.best[key] = S.vars[key]
           S.best_gen = S.gen       

        if abs(S.ALFA) < 1.0e-6:
            S.Sval = Sval = S.BETA
        else:
            S.Sval = Sval = gammavariate(S.ALFA,1./(S.error2 +S.BETA))
            #S.Sval = Sval = gammavariate(S.ALFA,(S.error2 +S.BETA))
        if PrintGen: 
            print S.gen,
            stdout.flush()

        if S.error2 < S.error1:
            S.Step = True

            for key in S.vars.keys():                         # SPLU
                S.accepted[key] = S.vars[key]                   # SPLU
            S.acceptgen = S.gen                            # SPLU	

            S.nfail = 0
            S.error1 = S.error2
        else:
            S.Temp = Temp = max(S.TZERO * exp(-S.AA*S.gen**S.BB),S.TMIN)

            pde = exp(-Sval*(S.error2 - S.error1)/Temp)

            if pde > random():
                S.bsistep = True

                for key in S.vars.keys():                         # SPLU
                    S.accepted[key] = S.vars[key]                   # SPLU
                S.acceptgen = S.gen                            # SPLU

                S.nfail = 0
                S.error1 = S.error2
            else:
                S.bsistep = False

                S.nfail += 1

        if S.error2 < S.errmin:
            S.errmin = S.error2
            for key in S.vars.keys():
                S.best[key] = S.accepted[key]
            S.bestgen = S.gen

        if S.nfail > S.NTARGET:
            S.jdict["converged"] = True

        for key in sorted(S.vars.keys()): # sorted for test; harmless for small number of keys
            itemrange = S.BOUNDS[key]
            if S.bsistep:
                S.vars[key] = S.newval(S.vars[key],itemrange)
            else:
                S.vars[key] = S.newval(S.accepted[key],itemrange)

        super(scheme,self).update()

        with open(self.dest + sep + self.PARMFNAM,"w") as parmfile:
            parmfile.write(self.TEMPLATE % self.vars)


    def setup(self,**kwargs):
        if self.jdict.get("converged",None):
            self.cleanup(self.dest)        
            raise proto.StopMECS


    def cleanup(self,dir):
        from os.path import isdir
        from os import chdir

        assert isdir(dir)
        chdir(dir+"/..")
        _("rm -rf %s" % dir)


    def newval(self,oldval,bounds):
        minm, maxm = bounds

        S = self
        count = 0
        while True:
            g = S.gen + 1 # !!!!
            T = max(S.TZERO * exp(-S.AA*g**S.BB),S.TMIN)
            #T = max(S.TZERO * exp(-S.AA*S.gen**S.BB),S.TMIN)
            result =  oldval + choice([-1.,1.]) * T * ( (1 + 1/T) ** random() - 1) * (maxm - minm)/2.
            if minm < result < maxm: break
            count += 1
            if count > 100: raise ValueError,"newval: bounds are too tight"
        return result
