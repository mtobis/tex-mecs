from sys import path
#for item in ("./MetaScheme","./Lister"): path.append(item)
#import lister
from os import sep,environ
from os import system as _
from os.path import dirname,abspath

path.append(environ.get("MECS_SCHEMES",dirname(dirname(__file__))+"/Lister"))
import lister

class scheme(lister.scheme):

    def __init__(self,jdict,logger,schemedir=None,sequence=None):
        if sequence is None:
            sequence = ["setup","Model.setup","Model.run","Model.backup","Cost.setup","Cost.run","Cost.backup","update"]
        if schemedir is None:
            schemedir = abspath(dirname(__file__))
        super(scheme,self).__init__(jdict,logger,schemedir=schemedir,sequence=sequence)
