# Lister

# The Model Ensemble Control System (also known as MECS or Tex-MECS) is
# Copyright (c) 2012 Board of Regents of the University of Texas
# 
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, 
# this list of conditions and the following disclaimer.
# 
# Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation 
# and/or other materials provided with the distribution.
# 
# Neither the name of the University of Texas nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
# THE POSSIBILITY OF SUCH DAMAGE.

from sys import path
from os import system as _
from os import sep,environ
from os.path import dirname,abspath, isfile, isdir

schema = environ.get("MECS_SCHEMES",dirname(dirname(__file__)))
path.append(schema+"/Proto")
import proto
from proto import Print, StopMECS

class scheme(proto.scheme):

    def __init__(self,jdict,logger,schemedir=None,sequence=None):
        if sequence is None:
            sequence = ["setup","Model.setup","Model.run","update"]
        if schemedir is None:
            schemedir = abspath(dirname(__file__))
        super(scheme,self).__init__(jdict,logger,schemedir=schemedir,sequence=sequence)
        try:
            assert isdir( jdict["_exp_parms"] + "/List/")
        except AssertionError:
            print "no such list file %s" % jdict["_exp_parms"] + "/List/"
            raise

    def setup(self,**kwargs):

        from os import sep

        jdict = self.jdict
        
        parmfnam = jdict.get("parmfnam","parms.dat")
        src = jdict["_exp_parms"] + "/List/"+parmfnam+".g.%04d"%int(self.gen)
        param_fq = self.dest+sep+parmfnam
        if not isfile(src):
            Print( "source %s not found. List presumed complete." % src)
            self.cleanup(self.dest)        
            raise StopMECS
        cmd = "cp %s %s" % (src,param_fq)
        try:
            assert not (_(cmd))
        except AssertionError:
            print "copy failed"
            raise IOError

    def cleanup(self,dir):
        from os.path import isdir
        from os import chdir

        assert isdir(dir)
        chdir(dir+"/..")
        _("rm -rf %s" % dir)
    
