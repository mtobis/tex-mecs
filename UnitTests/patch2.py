from glob import glob
from os import getcwd,chdir
from os import system as _

cwd = getcwd()
dirs = [dir for dir in glob("Ens*") ]
for dir in dirs:
    print dir
    chdir(dir)
    jspecnam = glob("*jspec")[0]
    with file(jspecnam) as jspec:
        text = jspec.read()
    text = text.replace("Models","Components")
    print text
    print 50*"="
    _("mv %s %s.old" % (jspecnam,jspecnam))
    with file(jspecnam,"w") as jspec:
        jspec.write(text)
    chdir(cwd)    
