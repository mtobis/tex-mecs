from os import system 
from sys import argv,exit

interactive = "-q" not in argv

def S(text): 
    #print text 
    system(text)

#def _(text): print text

def prompt():
    if interactive:
        return raw_input("\nhit any key to proceed\n")
    return

print("""
Running MECS tests.

Ensure environment variable MECSDIR is set before running.

Now setting up tests.
"""); 

from os import environ
if "SCRATCH" in environ:
    for experno in range(6,8):
        targ = "BC%d" % experno
        S("rm -rf %s/%s/*"%(environ["SCRATCH"],targ))

###############

print("""
1: Run Pseudo-CAM with a Break
"""); 

S("../mecs -s BC6")
print 80 * "="
S("ls $SCRATCH/BC6")
print 80 * "="

print("""
Above should run through g.0004
""")
 
prompt()


###############

print("""
2: Run Pseudo-CAM with zero progress
"""); 
S("../mecs -s BC7")
print 80 * "="
S("ls BC7")
print 80 * "="
print("""
Above should fail, yielding "No Progress" message.
""")
exit()
prompt()

###############

print("""
4. Detect Failure to Run
"""); 
S("./mecs -s Exper3")
print 80 * "="
S("ls TestRes3")
print 80 * "="
print "Above should not proceed beyond first generation yielding an Assertion Error"
prompt()

###################

from os.path import isdir

print("""
5: Setup should fail if output directories exist
""");
print 80 * "="
S("./premecs Exper3")
print 80 * "="
print("""
above should print "Target already exists" and exit.
""")
prompt()

##################

print("""
6. Multiple components.
"""); 
S("./mecs -s Exper6")
print 80 * "="
S("ls TestRes6")
print 80 * "="
print("""
above should run to g.0002 and exit
""")
prompt()

#########

from sys import exit
exit()

"""
from Util import __
hostname = __("hostname")
if "ls" in hostname and "tacc" in hostname:
    from os import environ
    scratch = environ["SCRATCH"]
    print '7. Running on SCRATCH'
    S("./mecs -s Exper7") 
    print 80 * "="
    S("ls %sTestRes7"%(scratch+"/"))
    print 80 * "="
    print 'above should run to g.0004 and exit.' 
    prompt()

##############

if "ls" in hostname and "tacc" in hostname:

    print '8. CAM dry run '
    S("./mecs -s Exper8")
    print 80 * "="
    S("ls %sTestRes8"%(scratch+"/"))
    print 80 * "="
    print 'above should run to g.0004 and exit.' 
    prompt()

#===
"""


