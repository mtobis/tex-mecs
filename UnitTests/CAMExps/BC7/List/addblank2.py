from glob import glob

count = 0

flist = glob("_*var")
flist.sort()

for fnam in flist:
    print fnam
    datafile = file(fnam)
    data = datafile.readlines()
    datafile.close()
    ofnam = "parms.dat.g.%04d"%count
    odatafile = file(ofnam,"w")
    for line in data:
        odatafile.write(" "+line)
    odatafile.close()
    count += 1
