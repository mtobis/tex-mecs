from glob import glob

PATTERN = "_0_*var"

flist = glob(PATTERN)

count = 0

print flist
for fnam in flist:
    outfnam = "parms.dat.d.%04d"%count
    with open(fnam) as f:
        ll = f.readlines()
        with open(outfnam,"w") as g:
            g.write("{\n")
            for l in ll:
                tokens = l.strip().split()
                outstr = '"' + tokens[0] + '": ' + tokens[2] 
                if l != ll[-1]:
                    outstr += ","
                outstr += "\n"
                g.write(outstr)
                print outstr
            g.write("}")
    count += 1
