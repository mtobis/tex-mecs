## R CODE to sequential design using ALC criterion ###

# Install library TGP
#install.packages(tgp, lib="/home/ucaksgu//R/x86_64-redhat-linux-gnu-library/2.11")
#library(maptree)

#load libraries
library(akima,lib.loc='/home/ucaksgu/R/x86_64-redhat-linux-gnu-library/2.11/')
library(tgp,lib.loc='/home/ucaksgu/R/x86_64-redhat-linux-gnu-library/2.11/')

#read files of options/parameters for R file

# numbcolpar.col: number of columns of parameters
# numbcolout.col: number of columns of outputs (1 is what can work for now with this!!)
# candid.num: number of candidates for parameters in new LHS
# number of runs per batch of sequential design: top
# number of iterations in MCMC: (burnin, end, thinning)
param.rfile<-read.table("param-Rfile.dat",header=TRUE)

# lowboundbounds for the parameters in new LHS
# col=parameters, rows: row1=lower bound, row2=upper bound
param.bounds<-as.matrix(read.table("param-bounds.dat",header=FALSE))

#read table of old runs of simulator: columns of simulator parameters & columns of outputs
old.runs<-read.table("oldruns.dat",header=TRUE)

#build a new LHS to explore where to sample for calib param
lhs.2step <- lhs(candid.num, t(param.bounds))


#faster: pred.n: ‘TRUE’ (default) value results in prediction at the inputs
#          ‘X’; ‘FALSE’ skips prediction at ‘X’ resulting in a faster
#          implementation

#   krige: ‘TRUE’ (default) value results in collection of kriging means
#          and variances at predictive (and/or data) locations; ‘FALSE’
#          skips the gathering of kriging statistics giving a savings in
#          storage
#
#    zcov: If ‘TRUE’ then the predictive covariance matrix is
#          calculated- can be computationally (and memory) intensive if
#          ‘X’ or ‘XX’ is large.  Otherwise only the variances (diagonal
#          of covariance matrices) are calculated (default).  See
#          outputs ‘Zp.s2’, ‘ZZ.s2’, etc., below

###################################################
# we start initial design X and then use lhs.2step XX

X<-old.runs[,(1:numbcolpar.col)]
XX<-data.frame(lhs.2step)
colnames(XX)<-colnames(X)

Z<-old.runs[,((numbcolpar.col+1):(numbcolpar.col+numbcolout.col))]

model.gp<-bgp(X, Z, XX = XX, meanfn = "constant", bprior = "bflat", corr = "expsep", BTE = c(400,1600, 2), R = 1, m0r1 = TRUE,  itemps = NULL, pred.n = FALSE, krige = FALSE, zcov = FALSE, Ds2x = TRUE, improv = FALSE, sens.p = NULL, nu = 1.5, trace = FALSE, verb = 4)

#ALC: model.gp$Ds2x ; #sort by ALC improved.
order.alc<-sort.list(model.gp$Ds2x,decreasing = TRUE)

# some uncertainties:
filename2.i<-paste("waccm-quantiles2-595-step",i,".txt",sep='')
write.table(cbind(model.gp$Zp.q1,model.gp$Zp.q2), file=filename2.i)

########update step
#select the first top combinations: XX[order.alc[1:top],]
XX.new<-XX[order.alc[1:top],]

filename.i<-paste("design2-step",i,".txt",sep='')
write.table(XX.new, file=filename.i)
