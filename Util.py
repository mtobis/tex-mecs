# Util.py


# The Model Ensemble Control System (also known as MECS or Tex-MECS) is
# Copyright (c) 2012 Board of Regents of the University of Texas
# 
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, 
# this list of conditions and the following disclaimer.
# 
# Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation 
# and/or other materials provided with the distribution.
# 
# Neither the name of the University of Texas nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
# THE POSSIBILITY OF SUCH DAMAGE.

import math
import subprocess as SP
from time import sleep

def __(cmd,terse=True,duration=100,maxincrement=10):
    p = SP.Popen(cmd,stdout=SP.PIPE,stderr=SP.PIPE,shell=True)
    sleep(4)
    r = p.poll()
    if r is not None:
        o,e = p.communicate()
    else:
	increment = 0
        steps = duration/maxincrement + int(math.log(2*maxincrement,2))
        for i in range(steps):
            sleep(increment)
            if not increment:
                increment = 1
            elif increment < maxincrement:
                increment = min(2*increment,maxincrement)
            r = p.poll()
            if r is not None:
                o,e = p.communicate()
                break
    if r is None:
        del p
        raise IOError, "process seems to be hanging"
    if e:
        if not terse: 
            print e
    if not terse:
        print 80*"-"
        print o
        print 80*"-"
    if r:
        del p
        raise IOError, "process return code %s" % str(r)
    del p
    return o


"""
def __(cmd):
    o,e = SP.Popen(cmd,stdout=SP.PIPE,stderr=SP.PIPE).communicate()
    if e:
        raise IOError,e
    return o
"""

def Print(arg,logger,level=None,verbose=True):
    if verbose:
        logger.info(arg)
    else:
        if not level:
            logger.debug(arg)
        else:
            if level == "!":
                logger.warn(arg)
            else:
                logger.info(arg)

def jload(jfile,promote=False):
    import json
    try:
        jthing = json.load(jfile)
    except ValueError:
        raise ValueError, "json file did not parse"
    return _jparse(jthing,promote)

def jloads(jstring,promote=False):
    import json
    try:
        jthing = json.loads(jstring)
    except ValueError:
        raise ValueError, "json string did not parse"
    return _jparse(jthing,promote)

def _jparse(jobj,promote = False):
    newobj = {}
    if isinstance(jobj,unicode):
	return str(jobj)
    if isinstance(jobj,str):
	return jobj
    if isinstance(jobj,int):
        return jobj
    if isinstance(jobj,float):
        return jobj

    """
    try:
        jobj.iteritems
    except AttributeError:
        import pdb;pdb.set_trace()
    """

    for key, value in jobj.iteritems():
        key = str(key)

        if isinstance(value, dict):
            newobj[key] = _jparse(value)
        elif isinstance(value, list):
            if key not in newobj:
                newobj[key] = []
                for i in value:
                    newobj[key].append(_jparse(i))
        elif isinstance(value, unicode):
            val = str(value)
            if val.isdigit():
                val = int(val)
            else:
                if promote:
                    try:
                        val = float(val)
                    except ValueError:
                        val = str(val)
            newobj[key] = val
        elif isinstance(value, str):
            val = str(value)
            if val.isdigit():
                val = int(val)
            else:
                if promote:
                    try:
                        val = float(val)
                    except ValueError:
                        val = str(val)
            newobj[key] = val
        else:
            newobj[key] = value
    return newobj

# via http://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-unicode-ones-from-json-in-python

if __name__ == "__main__":
    print "should work"
    __('ls %s'%__file__)
    print "ok"
    print "should fail"
    try:
        __('ls MxYzPtLk')
        print "you should not see this line"
    except IOError:
        print "failed as expected"
    print "should time out"
    try:
        __('sleep 100',duration=20,increment=5)
        "you should not see this line"
    except:
        print "timed out as expected"
    # test jload 
