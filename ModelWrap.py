#!/usr/bin/env python


# The Model Ensemble Control System (also known as MECS or Tex-MECS) is
# Copyright (c) 2012 Board of Regents of the University of Texas
# 
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, 
# this list of conditions and the following disclaimer.
# 
# Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation 
# and/or other materials provided with the distribution.
# 
# Neither the name of the University of Texas nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
# THE POSSIBILITY OF SUCH DAMAGE.
Verbose = False

"""
ModelWrap.py
    
    here is an example of a doctest

    >>> 1 + 1
    2

NOTE: Ensure ./UnitTests/MECSTEST/modelwraptest exists at test time

"""

class ModelWrap(object):
    r"""
    ====

    instantiate:
    >>> from os.path import isdir,dirname,abspath
    >>> thisdir = dirname(abspath(__file__))
    
    >>> mmm = ModelWrap(thisdir + "/UnitTests/MECSTEST/modelwraptest/")

    ====

    We auto-wrap all shell scripts as methods, so e.g.,
    
    ModelWrap_instance.compile("some command line stuff") 

    passes 
    "compile.sh some command line stuff"
    to the shell (if subsc is ".sh", which is the default) 

    this allows the user to write shell scripts and have them be called by python

    ====

    we test if the instance knows how to do certain things

    >>> mmm.can("test")
    'test.sh'

    >>> mmm.can("fly_like_a_bird")

    empty result

    >>> mmm.test()
    foo
    <BLANKLINE>

    >>> mmm.__getattr__("test")()
    foo
    <BLANKLINE>

    if the file doesn't exist an OSError is raised

    >>> mmm.notfound() #doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    OSError: 127 No such file or directory

    if the command fails a nonzero result is propagated 

    >>> mmm.fail() #doctest: +IGNORE_EXCEPTION_DETAIL
    <BLANKLINE>
    'error fail.sh\nThis routine always fails.: '

    Parameters are successfully passed

    >>> mmm.longest('a','bb','ccc')
    ccc
    <BLANKLINE>

    >>> mmm.longest('a bb ccc')
    ccc
    <BLANKLINE>

    """

    def __init__(self,modelpath="."):
        from os.path import abspath, isdir
        self.modelpath = abspath(modelpath)
        assert isdir(self.modelpath)
     
    def __getattr__(self,prognam):
        from os import sep,environ,system, chdir
        import subprocess as SP

        progname = self.can(prognam)
        
        if not progname:
            raise OSError, "program %s not found" % prognam

        def tmp(*cmds,**kwargs):

            #0/0
            if "context" in kwargs:
                chdir(kwargs["context"])
            if progname.endswith(".py"):
                if len(cmds) == 1 and isinstance(cmds[0],str):
                    cmds = cmds[0].split()
                exe = ["python", self.modelpath + sep + progname] + list(cmds)
                p = SP.Popen(exe, shell=False, stdout=SP.PIPE, stderr=SP.PIPE)
            elif progname.endswith(".sh"):
                exe = " ".join([self.modelpath + sep + progname] + list(cmds))
                p = SP.Popen(exe, shell=True, stdout=SP.PIPE, stderr=SP.PIPE)         
            else:
                if len(cmds) == 1 and isinstance(cmds[0],str):
                    cmds = cmds[0].split()
                exe = [self.modelpath + sep + progname] + list(cmds)
                p = SP.Popen(exe, shell=False, stdout=SP.PIPE, stderr=SP.PIPE)

            res, errms = p.communicate()
            err = p.wait()
            
            """
            if err:
                if errms:
                    raise OSError, str(err) + "\n" + errms
                else:
                    raise OSError, str(err)
            return res
            """
            #0/0
            #import pdb; pdb.set_trace()
            #if not "terse" in kwargs:
            #    print "=== === ===>"+str(res)+"<"
            if Verbose: print res

            if err:
                #print "res %s" % res
                #print "error %s" % err
                try:
                    return "error %s: " % (str(exe.split(sep)[-1]) + "\n" + errms)
                except:
                    return "error %s: " % (exe[-1] + "\n" + errms)

            else:
                return None

        #import pdb
        #pdb.set_trace()
        
        return tmp
        #lambda: system(tmp())

    def can(self,progname):
        from os import sep
        from os.path import isfile
        
        if isfile(self.modelpath+sep+progname):
            return progname
        for suffix in ".sh .py".split():
            if isfile(self.modelpath+sep+progname+suffix):
                return progname+suffix
        return None


if __name__ == "__main__":

    from sys import argv
    from os.path import isdir,dirname,abspath

    thisdir = dirname(abspath(__file__))

    assert isdir(thisdir + "/UnitTests/MECSTEST/modelwraptest/")
    if "-test" in argv:
        Verbose = True
    import doctest
    doctest.testmod()
