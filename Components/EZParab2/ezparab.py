#!/usr/bin/env python
from random import seed, gauss

with file("params.dat") as parms:
    for line in parms.readlines():
        exec( "%s = %s" % tuple(line.strip().split()) )

seed(gen)

noise = gauss(0,sigma)
result = noise + (xx - atarg) ** 2 + (yy - btarg) ** 2  

with file("result.dat","w") as resfile:
    resfile.write(str(result)+"\n")

