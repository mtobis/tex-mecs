#!/usr/bin/env python
#from os import system, chdir,getcwd
#from glob import glob
from sys import argv
from sys import path
from os.path import dirname,abspath,isfile
from os import environ

path.append(environ.get("MECSDIR",dirname(abspath(__file__))))
from Util import __,jload

state = jload(file("state.json"))
runname = state.get("_CAM_runname","camrun.bsi")
lastmo = state.get("_CAM_lastmo","0020-09")

o = __("tar -xvf /work/00671/tobis/CAM-Sample-Out/camres.tar")
#assert glob("*cam2.h0*") # TEST SHOULD TAKE SOME PARAMETERS
assert isfile("%s.cam2.h0.%s.nc"%(runname,lastmo))
