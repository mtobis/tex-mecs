from os import environ
from os.path import abspath,dirname
from sys import path

path.append(environ.get("MECSDIR",dirname(abspath(__file__))+"../.."))
from Util import __
from jload import jload

state = jload(file("state.json"))

if state.get("Production",False):
    __("qsub < qsubprod",duration=86400,maxincrement=1200)
else:
    __("qsub < qsubscript",duration=3600,maxincrement=120)
