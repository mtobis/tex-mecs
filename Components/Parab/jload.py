# RECURSIVE JCASTER

Verbose = False

def jload(jfile,promote=False):
    import json
    jthing = json.load(jfile)
    if Verbose: print jthing
    return _jparse(jthing,promote)

def jloads(jstring,promote=False):
    import json
    jthing = json.load(jstring)
    if Verbose: print jthing
    return _jparse(jthing,promote)

def _jparse(jobj,promote = False):
    newobj = {}
    if Verbose: print "JJ"+str(jobj)

    if isinstance(jobj,unicode):
	return str(jobj)
    if isinstance(jobj,str):
	return jobj

    for key, value in jobj.iteritems():
        if Verbose: print "K", key,value
        key = str(key)

        if isinstance(value, dict):
            if Verbose: print "indict"
            newobj[key] = _jparse(value)
        elif isinstance(value, list):
            if Verbose: print "list!"
            if key not in newobj:
                newobj[key] = []
                for i in value:
                    newobj[key].append(_jparse(i))
        elif isinstance(value, unicode):
            if Verbose: print "+" + newobj
            val = str(value)
            if val.isdigit():
                val = int(val)
            else:
                if promote:
                    try:
                        val = float(val)
                    except ValueError:
                        val = str(val)
            newobj[key] = val
            if Verbose: print "++" + newobj
        elif isinstance(value, str):
            if Verbose: print "S+" + newobj
            val = str(value)
            if val.isdigit():
                val = int(val)
            else:
                if promote:
                    try:
                        val = float(val)
                    except ValueError:
                        val = str(val)
            newobj[key] = val
            if Verbose: print "++" + newobj
        else:
            newobj[key] = value
    return newobj

# via http://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-unicode-ones-from-json-in-python
