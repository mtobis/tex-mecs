def paraboid(x, y, a = 3.0, b= 4.0, c = 2.0, d= 2.0, noisamp=0.):
    return(a*(x-b)**2 + c*(y-d)**2 + mynoise(noisamp))

def mynoise(noisamp):
    from random import gauss
    return noisamp * gauss(0.,1.)

Verbose = False

if __name__ == "__main__":

    from sys import argv

    if len(argv) > 1:
        fnam = argv[1]
    else:
        fnam = "parms.dat"
    parms = file(fnam,"r")

    import json
    try:
        from jload import jload
    except ImportError:
        from sys import path
        from os import environ
        from os.path import abspath,dirname
        path.append(environ.get("MECSDIR",abspath(dirname(__file__))+"/../.."))
        from Util import jload
    pdict = jload(parms)

    for key in pdict.keys():
        globals()[str(key)] = pdict[key]

    result = paraboid(x,y)

    if Verbose: print result
    output = file("result.dat","w") 
    output.write(str(result)+"\n")
    output.close()

