from os import environ
from os.path import abspath,dirname
from sys import path

from glob import glob

path.append(environ.get("MECSDIR",dirname(abspath(__file__))+"../.."))
from Util import __
from jload import jload

state = jload(file("state.json"))

from sys import exit
if not state.get("Production",False):
    with file("result.dat","a") as final:
        final.write("COST\n")
        final.write("99.99\n")
        exit()

#camrun.bsi.cam2.h0.0009-04.nc

pattern = "camrun.cam2.h0.002[1-4]-*"
prefix =  "camrun.bsi.cam2.h0.0019-"

seasons = {
"DJF" : ["12","01","02"],
"MAM" : ["03","04","05"],
"JJA" : ["06","07","08"],
"SON" : ["09","10","11"]
}

# seasons["ANN"] = sum([seasons[season] for season in seasons],[])

allfiles = glob(pattern)

for season in seasons:
    allmos = ""
    for month in seasons[season]:
        allmos += " ".join(glob(pattern+month+".nc"))
        allmos += " "
    dest = prefix + season + ".nc"
    cmd = "ncea %s %s" % (allmos,dest)
    print cmd
    __(cmd,)

__("/work/00671/tobis/F2/bin/ferret -script runsome",duration=1000,maxincrement=100)

with file("result.dat") as raw:
    data = raw.readlines()

count = 0
total = 0
for line in data:
    try:
        val = float(line.strip())
    except:
        continue
    count += 1
    total += val

with file("result.dat","a") as final:
    final.write("COST\n")
    final.write(str(total/count)+"\n")


