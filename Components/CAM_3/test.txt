.. include:: <s5defs.txt>

============================
 Computation at UTIG and BEG
============================

 Michael Tobis
 TACC
 Austin TX
 Jan 12 2009

.. |bullet| unicode:: U+02022

.. footer:: Austin TX |bullet| Jan 12 2009


Introduction
============

Jackson School of Geosciences

 1. Department of Geological Sciences (main campus)
 2. Bureau of Economic Geology (J J Pickle)
 3. Institute for Geophysics (J J Pickle)

Possibly the largest earth sciences program at any university.

Divisions
=========

Divisions have separate histories, pulled together as a college in
2005.

Institute for Geophysics ("UTIG"): Mounts field research expeditions

Bureau of Economic Geology ("BEG"): State and corporate applied research

Computation doesn't fit in perfectly. "Computational geosciences"?

Me
==

Computer scientist by inclination

Climatologist by training

Whole systems/sustainability writer by avocation

Wandered out of and back into academia 

Funding got me here as a climate scientist

Limited funding moved me half time into exploration geophysics

So
==

I have been an advocate for a formal liason between disciplines and
compute centers

Two of the main users of high end computing in geosciences

"The customer does not know what is possible." -Akio Morita

converse of Morita's principle

Part I
======

Climatology

Computational Climatology
=========================

.. image:: ./CLIMATE8.gif

Community Modeling Process
==========================

Atmospheric and oceanic sciences based on community models

Believed too large for an individual or small group effort

(Small models in Matlab expected to yield qualitative results only)

"Dynamics" (fluid flow) and "Physics" (everything else)

examples used here: WRF, MOM, CAM, CCSM

Rarely used as initial value problems. We care about the shape of the
trajectory, not about the individual points. (Weather vs climate)

Community Model Characteristics
===============================

FORTRAN-90

Software engineering improving, but affected by FORTRAN legacy.

Poorly documented, difficult for end user to install.

Small memory demands; limited scalability (256 CPUs).

Lengthy runs; fortunately checkpointing works nicely.

Very large output files; output data management is problematic.

Mismatch between Design and Use Cases
=====================================

Design use case: change a few known parameters and run for a century
of simulated time. Publish for IPCC reports. Release to community.

Usual use case: hacking into the code.

Users very smart but untutored; e.g., know nothing about version
control, build systems.

Multiple experiments, weak support for file management can lead to
confusion, errors, wasted runs.

Ensemble Control Problem
========================

Interest in tuning weakly constrained model physics by comparing
against observations.

Parameter space, say, 6-dimensional.

Require a simulated decade for a climate to emerge from noise = 32 CPU
x 1 day on Lonestar.

Next run is based on output of previous run

Under control of a Python script on head node which mostly sleeps

Failure of script; failure of processor nodes; completion of script =>
nontrivial control code.

The Future
==========

Currently handle totally different chains of executions with dofferent
script instances; it's possible to imagine more complex search strategies

Given an exaflop I will mostly want to run a million gigflop codes. 

If my jobs don't scale beyond 256 nodes or so I will want a very
complex control script

Current script mechanisms inadequate as far as I know

Multiple Executable Problem
===========================

.. image:: framework.gif


Multiple Executable Problem
===========================

Historically implemented by separate groups; distinct development
strategies; in principle may be targeted to different compiler options
or even different compilers

I am interested in building a scripted coupler to facilitate new
combinations

Multiple executable has proven to be a nonstandard use case

NCAR has buckled; new CCSM release to be single-executable

Is this progress? (I don't think the supercomputing centers have even
noticed this problem.

Other Climate Work at UTIG
==========================

Kerry Cook & Ned Vizy builds on WRF, runs in climate mode (at NCAR)

Liang Yang embeds WRF in CAM

Brian Arbic uses POP

Rob Scott uses MOM (also may have some original code)

Everyone analyzes very large model output datasets, usually in Matlab

Similar Features
================

Community F90 models

Moderate scalability; communication bound. Not memory constrained.

Very large output datasets. Lack of tools for parallel
analysis. Pushing conventional desktop tools very hard, or home-brewed
F90, Matlab, or meteorology/oceanography postprocessing DSLs (Ferret, NGL).

Not Matplotlib + Python, mysteriously.

Part II
=======

Exploration Geophysics
======================

Problem: Tomography (Non-Invasive Testing)

The Observation
===============

.. image:: ./seis.png

Migration
=========

(x,y,t) space to (x,y,z) space

actually ((x1,y1),(x2,y2),t) space to (x,y,z) space

seeking to identify discontinuities in subterranean structure

there's much postprocessing but this is the rate limiting step

Very Different Usage Patterns
=============================

"3D calculations" becoming important

Very large memory footprint

Very small calculations per data point

Essentially zero communication (embarassingly parallel) 

(Tradeoff between subdomain size and accuracy)


This is an Enormously Important Use Case
========================================

50 of the top 500 machines exclusively dedicated to this problem

Universities typically cannot affrod these calculations

Access to TACC facilities can give UTIG a real leg up

Two Subclasses
==============

home-brew (mostly algorithm research)

commercial (Paradigm Geoframe) (mostly geology research)

the latter has run on Lonestar but is memory constrained there

Interest in Ranger but no further attempt to run on TACC platforms

Madagascar
==========

A UT product, among its ambitions is to compete with commercial
seismic analysis software

Support for massive parallelism would be a big win

Allocating the CPUs doesn't fit in to the rule-based scripting
mechanism easily

Idiosyncratic queuing mechanisms and lack of access to alternative
platforms hinder development of parallel tools

Part III
========

A project that combines all the above:

West Antarctic Ice Sheet Modeling
=================================

Radar observations of the Antarctic ice sheet need inversion

Ice sheet dynamics model needs building

Ice sheet flow needs to be inverted

Ice dynamics identified

Prediction of sea level rise

Computational footprint
=======================

All three parts:

 * the very large memory, small computational footprint
 * the small memory, high computation continuum model
 * the ensemble-driven inversion


Summary
=======

End user views


Missing ideas
=============

Distributed computing; virtualized resources invisible

Grid seen as a bureaucratic impediment and not an enabler

Very limited understanding of software tools


Good News
=========

Very enthusiastic about proximity to resources

Eager for closer collaboration

Probably open to CiSE etc. proposals that directly advance geophysics


Relationship to TACC
====================

Comments from others:

* When I tried to port code to TACC, they were not helpful at all. I
  would have appreciated more help getting it compiled and running. It
  turns out someone was already running (the same model), so it shouldn't have been so difficult for them.

* If there's someone there who I could approach about, saying, solving an elliptic eqn in irregular domain, that would be wonderful.

* User groups would be useful, so we could develop collective
  knowledge base; Gotchas need to be caught and documented

* No useful way to manage large output datasets; "babysitting"


Upbeat Ending
=============

We have an unusual opportunity: two high profile operations in close
proximity

Potential for very interesting collaborations and high profile
successes

