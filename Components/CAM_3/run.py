
"""
#!/usr/bin/env python
#from os import system, chdir,getcwd
#from glob import glob
from sys import exit
from sys import path
from os.path import dirname,abspath,isfile
from os import environ,sep
from glob import glob

from os import getcwd
print getcwd()

===
from sys import exit
exit()
===

path.append(environ.get("MECSDIR",dirname(abspath(__file__))))
from Util import __,jload

SOURCE = "/work/00671/tobis/CAM-Sample-Out/camres.tar" # so old lonestar tests will still work
                                                       # can be removed from distribution
state = jload(file("state.json"))
runname = state.get("$runname","camrun.bsi")
lastmo = state.get("$lastmo","0019-12")
pattern = state.get("$pattern","*.cam2.h0.*")
source = state.get("$source",SOURCE)
expdir = state["_expdir"]


import logging
#logging.basicConfig(filename=expdir+sep+__file__.split(sep)[-1]+"mecs.log",level=logging.DEBUG)
logger = logging.getLogger('BoCAM2 run.py')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(expdir+sep+abspath(__file__).split(sep)[-2]+"_mecs.log")
#import pdb;pdb.set_trace()
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

def Print(arg,level=None):
    if not level:
        logger.info(arg)
    else:
        if level == "!":
            logger.warn(arg)
        else:
            logger.info(arg)

#=== SIMULATING CAM
yrpairs = state.get("$yrpairs",None)
if not yrpairs:
    yrpairs=((10,19),)
else:
    yrpairs = eval(yrpairs)
Print("Pseudo-CAM year pairs: "+str(yrpairs))

def allmos(firstyr,lastyr):
    mofnam = "mos.txt"
    mofile = open(mofnam,"w")
    for yr in range(firstyr,lastyr+1):
        for mo in range(1,13):
            fnam = "%s.cam2.h0.%04d-%02d.nc\n"%(runname,yr,mo)
            mofile.write(fnam)
    mofile.close()           
#===

completed = None

if isfile("progress"):
   with file("progress") as progress:
      try:
          lastfile = progress.readlines()[-1].strip() 
      except IndexError:
          Print("empty extant progress file","!")
          lastfile = None
else:
   o = __("touch progress")
   lastfile = ""

done = False
stuck = False

while not done and not stuck:

#===
# SIMULATING CAM

    with file("iter","a") as iteration:
        iteration.write("+")
    with file("iter") as iteration:
        iterdat = iteration.read()
        Print("->"+iterdat+"<-")
        itercount = iterdat.count('+')
        Print("iter: "+str(itercount))
    try:
        yrpair = yrpairs[itercount-1]
    except IndexError:
        Print("No More File Extractions Specified","!")
        done = True
    Print(str(yrpairs))
    Print(str(yrpair))
    allmos(*yrpair)

    Print(getcwd())
    from os.path import isfile
    if isfile("SKIPTAR"): 
        Print("Skipping tar")
    else:
        Print('tar -xvf %s -T"mos.txt"'%source)
        o = __('tar -xvf %s -T"mos.txt"'%source,duration=600,maxincrement=20)
    

#===
    done = done or isfile("%s.cam2.h0.%s.nc"%(runname,lastmo))
    completed = sorted(glob(pattern))[-1]
    Print(">"+lastfile+"<","+")
    Print(">"+completed+"<","+")
    if completed == lastfile:
        stuck = True
    else:
        lastfile = completed
        with file("progress","a") as progress:
            progress.write(completed + "\n")

if stuck:
    Print("Could not complete run.","!")
    exit(1)

Print("run.py normal exit","+")

"""

from os import environ,sep
from os.path import abspath,dirname
from sys import path
from os.path import isfile

path.append(environ.get("MECSDIR",dirname(abspath(__file__))+"../.."))
from Util import __
from jload import jload


def run(state):
    if state.get("Production",False):
        __("qsub < qsubprod",duration=86400,maxincrement=1200)
    else:
        __("qsub < qsubscript",duration=3600,maxincrement=120)

state = jload(file("state.json"))
runname = state.get("$runname","camrun")
lastmo = state.get("$lastmo","0024-12")
pattern = state.get("$pattern","*.cam2.h0.*")
expdir = state["_expdir"]

import logging
#logging.basicConfig(filename=expdir+sep+__file__.split(sep)[-1]+"mecs.log",level=logging.DEBUG)
logger = logging.getLogger('CAM2 run.py')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(expdir+sep+abspath(__file__).split(sep)[-2]+"_mecs.log")
#import pdb;pdb.set_trace()
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
if state.get("Verbose",0):
    logger.addHandler(ch)
logger.addHandler(fh)

def Print(arg,level=None):
    if not level:
        logger.info(arg)
    else:
        if level == "!":
            logger.warn(arg)
        else:
            logger.info(arg)

def fixrpointers():
    from glob import glob
    flist = glob("*rpoint*")
    for fnam in flist:
        with file(fnam) as rpfile:
            text = rpfile.readlines()
        __("rm %s"%fnam)
        with file(fnam,"w") as rpout:
            for line in text:
                if sep in line:
                    line = "." + sep + line.split(sep)[-1]
                rpout.write(line)

if isfile("progress"):
   with file("progress") as progress:
      try:
          lastfile = progress.readlines()[-1].strip() 
      except IndexError:
          Print("empty extant progress file","!")
          lastfile = None
else:
   o = __("touch progress")
   lastfile = ""

done = False
stuck = False

while not done and not stuck:
    fixrpointers()
    run(state)
    done = done or isfile("%s.cam2.h0.%s.nc"%(runname,lastmo))
    completed = sorted(glob(pattern))[-1]
    Print(">"+lastfile+"<","+")
    Print(">"+completed+"<","+")
    if completed == lastfile:
        stuck = True
    else:
        lastfile = completed
        with file("progress","a") as progress:
            progress.write(completed + "\n")

if stuck:
    Print("Could not complete run.","!")
    exit(1)

Print("run.py normal exit","+")

