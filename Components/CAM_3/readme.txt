Do a spinup run. Then if run is not complete, do a restart. 

This will demonstrate more of what the responsibilities of the setup.py script
are.

First step is to verify that the rpointers are written in the cwd.

Once that works, setup should verify existence of rpointer and switch to
restart.
