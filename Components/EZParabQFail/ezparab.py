#!/usr/bin/env python

from random import seed, gauss
from sys import path
from os import environ,sep,getcwd

path.append(environ["MECSDIR"])

with file("params.dat") as parms:
    for line in parms.readlines():
        exec( "%s = %s" % tuple(line.strip().split()) )

try:
    gen = int(getcwd().split(sep)[-1].split(".")[-1])
except:
    print 'defaulting to zero'
    gen = 0

seed(gen)

noise = gauss(0,sigma)
result = noise + (xx - atarg) ** 2 + (yy - btarg) ** 2  

f = file("result.dat","w")
f.write(str(result))
f.close()    

