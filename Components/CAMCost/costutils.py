# Adapted for numpy/ma/cdms2 by convertcdms.py
'''
costutils.py
Srivatsan Ramanujam,18 Mar 2009
vatsan@cs.utexas.edu
Contains a common set of utilities required for the cost function computation
'''
import cdms2 as cdms,MV2 as MV,numpy.oldnumeric.ma as MA,numpy.oldnumeric as Numeric
from regrid2 import Regridder
from math import *
import cdutil
#from cdutil.vertical import linearInterpolation,reconstructPressureFromHybrid

    
def spatialRegridding(model,ref,overRideRef=0):
    '''
    Performs linear interpolation so that the grid of model is mapped to the same resolution as grid of ref
    '''
    #print 20*"="
    #print model
    print type(model)
    print 20*"="
    #import pdb; pdb.set_trace()
    modelGrid = model.getGrid()
    refGrid = ref.getGrid()  
    
    #When this flag is set, the reference file contains implicit mask...so to make use of this, we unconditionally
    #regrid the model grid to the reference grid (even though the ref grid may be of higher resolution)
    if(overRideRef==1):
        regridfunc = Regridder(modelGrid,refGrid)
	model = regridfunc(model)
	return model,ref
        
    #Regrid from the one which has more points (higher resolution) to the one which has fewer points

    #refGrid is of higher resolution
    if(MV.size(refGrid)>MV.size(modelGrid)):
         regridfunc = Regridder(refGrid,modelGrid)
	 ref = regridfunc(ref)
    #modelGrid is of higher resolution	 
    else:
         regridfunc = Regridder(modelGrid,refGrid)
         model = regridfunc(model)
    print "."
    return model,ref
    
def getPressureAvg(var,lat,lon,P0,PS,hyai,hybi):

    #Pressure is not a hybrid axis, so directly average??
    #This is the case with the reference file
    
    #Use "object is None" check, coz there seems to be a weird issue with cdms none type (they distinguish between an array of none types and none
    #as such and there is no direct way to check if a value is none or an array contains none values
    if(hyai is None or hybi is None):
	#Implement the new stuff
	P = var.getAxis(1).getValue()
	DP = []
	DP.append((P[0]+P[1])/2)
        lev_max = len(P)
	nLat = PS.shape[0]
	nLon = PS.shape[1]
	ones = MA.ones((nLat,nLon))
	for lev in range(1,lev_max-1):
	    val  = ((P[lev+1]-P[lev-1])/2.0)*ones
	    DP.append(val)
	#
	val = (PS-(P[lev_max-1]+P[lev_max-2])/2)
	DP.append(val)
	#print '##DP:',DP
 
	varNum = None     
	for lev in range(len(DP)):
	    if(varNum is None):
	       varNum=(var[0][lev]*DP[lev]/PS)
	       continue
	    varNum += (var[0][lev]*DP[lev]/PS)
	return varNum	     
    else:	  
        #This might happen with the model file	
        dpres = []
        #Number of levels in the "lev" axis
        nlevels = len(var.getAxis(1))
        dpres = []
        sum_dpres=None
        for lev in range(nlevels):
           dpres.append((PS*hybi[lev+1]+hyai[lev+1]*P0) - (PS*hybi[lev] + hyai[lev]*P0))
	   if(sum_dpres is None):
	      sum_dpres = dpres[0]
	      continue
	   sum_dpres +=dpres[-1]
	  
    	
        varNum = None
        for lev in range(nlevels):
            if(varNum is None):
	       varNum = var[0][0]
	       continue	    	    
            varNum+=(var[0][lev]*dpres[lev])

	final = varNum/sum_dpres   
        return final

def linearInterpolationAtLevel(var,PS,hyai,hybi,P0,level):  
    Pres = []
    #First calculate pressure at all levels for each lat,lon
    for lev in range(len(hyai)):
        Pres.append(hyai[lev]*P0+hybi[lev]*PS)
    
    #Linearly interpolate U winds where appropriate
    varNum = None
    pLevels = len(var.getAxis(1))
    MK=None
    for lev in range(pLevels-1):
        #All positions having value > 30000
        mk1 = MA.greater(Pres[lev+1],30000.0)
	#All positions having value <=30000
	mk2 = MA.less_equal(Pres[lev],30000.0)
	#All positions in Pres[lev] where the values are <= PS
	mk3 = MA.less_equal(Pres[lev],PS)
	#All positions where all 3 of the above conditions are satisfied
	mk = mk1*mk2*mk3
	#The lat,lon values which should not be changed by the interpolation in the current level
	mk_compliment = MA.equal(mk,0)
	if(MK==None):
	  MK=mk
	else:  
	  MK+=mk
	
	if(varNum is None):
	   varNum = mk*(var[0][lev+1]+ (var[0][lev]-var[0][lev+1])*(30000.0-Pres[lev+1])/(Pres[lev] - Pres[lev+1]))
	   continue
	varNum = varNum*mk_compliment + mk*(var[0][lev+1]+ (var[0][lev]-var[0][lev+1])*(30000.0-Pres[lev+1])/(Pres[lev] - Pres[lev+1]))

    return varNum

    
def getWeights(model,ref):
    '''
    Returns NormCosWgtGlobe for latitudes
    '''
    #WHAT IS USUALLY PASSED IN ARE ALREADY THE LATITUDE WEIGHTS    
    model_lat_wt = model.getGrid().getWeights()[0]
    
    '''
    deg_to_rad = acos(-1.0)/180.0
    model_lat_wt = MV.cos(model_lat_wt*deg_to_rad)
    model_lat_wt_sum = MV.sum(model_lat_wt)
    model_lat_wt = 2.*model_lat_wt/model_lat_wt_sum
    '''
    ref_lat_wt = ref.getGrid().getWeights()[0]
    
    '''
    ref_lat_wt = MV.cos(ref_lat_wt*deg_to_rad)
    ref_lat_wt_sum = MV.sum(ref_lat_wt)
    ref_lat_wt = 2.*ref_lat_wt/ref_lat_wt_sum
    '''
    #taking cosine makes the weights almost identical??? whats the point then?
    
    return model_lat_wt,ref_lat_wt
            
def getModelWeights(model):
    '''
    Returns NormCosWgtGlobe for latitudes
    '''
    #Extract latitude weights from the grid and make it conform to the dimensions of obs/ref data matrices
    #This is done by merely copying the values for every other dimension
    model_lat_wt = model.getGrid().getWeights()[0]
    return model_lat_wt
          
def transposeAndConform(vector,nCols):
    '''
    Given an input row vector, it takes its transpose, and repeats the result column vector "nCols" number of times
    '''
    mat = MV.zeros((len(vector),nCols))*1.0
    for k in range(len(vector)):
        row = MV.ones(nCols)	
        mat[k] = vector[k]*row	
    return mat
        
def getMean(lst):
    '''
    returns the sample mean
    '''
    mean=0
    #1-d vector
    if(len(lst.shape)==1):
       mean = MV.sum(lst)/MV.count(lst)
    #2-d matrix (lat,lon)	
    else:
       mean = MV.sum(MV.sum(lst))/MV.count(lst)
    return mean

def getStd(lst):
    '''
    returns the sample std deviation
    '''
    mean = getMean(lst)
    std = 0
    #1-d vector
    if(len(lst.shape)==1):
       std = (MV.sum(MV.power((lst - mean),2))/MV.count(lst))**0.5
    #2-d matrix	
    else:
       std = (MV.sum(MV.sum(MV.power((lst - mean),2)))/MV.count(lst))**0.5	
    return std        
