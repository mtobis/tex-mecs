from os import environ,sep
from os.path import abspath,dirname
from sys import path

path.append(environ.get("MECSDIR",abspath(__file__+"/../../..")))
from Util import __,jload

state = jload(file("state.json"))
runname = state.get("$runname","camrun.bsi")
yrs = state.get("$cost_yrs","4 2")
pattern = runname + ".cam2.h0."
expdir = state["_expdir"]

lastyr = sum(map(int,yrs.split()))


import logging
#logging.basicConfig(filename=expdir+sep+__file__.split(sep)[-1]+"mecs.log",level=logging.DEBUG)
logger = logging.getLogger('BoCAM2 run.py')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(expdir+sep+abspath(__file__).split(sep)[-2]+"_mecs.log")
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

def Print(arg,level=None):
    if not level:
        logger.debug(arg)
    else:
        if level == "!":
            logger.warn(arg)
        else:
            logger.info(arg)



#o = __("python ensemble_averager.py . camrun.bsi.cam2.h0. 9 10",duration=1000)
Print ("python ensemble_averager.py . %s %s"%(pattern,yrs),"+")

o = __("python ensemble_averager.py . %s %s"%(pattern,yrs),duration=1000)

Print("unset PYTHONPATH; unset PYTHONHOME; /work/00671/tobis/CDMS/bin/python getcost.py . /work/00671/tobis/Obs std.txt %d result"%lastyr,"+")
o = __("unset PYTHONPATH; unset PYTHONHOME; /work/00671/tobis/CDMS/bin/python getcost.py . /work/00671/tobis/Obs std.txt %d result"%lastyr,duration=1000)
