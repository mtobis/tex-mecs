import MV
from costutils import *

def calcCost(obs,ref,wt):
    '''
    Computes the following metrics, when provided with a predicted list of values and a reference list of values
    std_obs = standard deviation of the predicted list, normalized with respect to itself
    std_ref = standard deviation of the reference list, normalized with respect to std_obs
    R = Correlation Coefficient between obs and ref
        R = 1/N(sum_n=1 to N {(obs_n - mean(obs))*(ref_n - mean(ref))}/(std_obs_unnormalized*std_ref_unnormalized)
    E' = RMSE computed on the mean centered data, normalized with respect to the standard deviation of the reference list
        E' = {(1/N) {sum_n=1 to N[(obs_n - mean(obs))-(ref_n-mean(ref))]^2}^(1/2)}/std_ref
    Bias = mean(obs) - mean(ref)
    '''
    #Time dimension is superfluous
    if(len(obs.shape)==len(ref.shape)==3):
       obs=obs[0]
       ref=ref[0]
       
    std_ref = getStd(ref)
    std_obs = getStd(obs)/std_ref
    R = computeCorrelation(obs,ref)
    E_prime = computeRMSEonMeanCentered(obs,ref)/std_ref
    MSE = computeMSE(obs,ref,wt)
    u1 = getMean(obs)
    u2 = getMean(ref)
    Bias = abs((u1+u2)/u2)*100

    result = {'std_obs':std_obs,'std_ref':std_ref,'R':R,'E_prime':E_prime,'Bias':Bias,'MSE':MSE}
    return result
    

def computeCorrelation(obs,ref):
    '''
    Computes the pearson correlation between the obs list and the ref list
    '''
    R=0
    if(obs.shape!=ref.shape):
        print 'obs:',obs,' ref:',ref
        raise '#Dimension of input lists donot match'
	
    obs_mean = getMean(obs)
    ref_mean = getMean(ref)
    obs_std = getStd(obs)
    ref_std = getStd(ref)
    #1-d vector
    if(len(obs.shape)==1):
        R = MV.sum((obs-obs_mean)*(ref-ref_mean))/(obs_std*ref_std*MV.count(obs))
    #2-d matrix	
    else:
        R = MV.sum(MV.sum((obs-obs_mean)*(ref-ref_mean)))/(obs_std*ref_std*MV.count(obs))	
    return R
	 
def computeMSE(obs,ref,wt):
    '''
    returns the MSE between the  obs and ref input lists
    '''
    if(obs.shape!=ref.shape):
        raise '#Dimension of input lists donot match'
	
    MSE=0
    #1-d vector
    if(len(obs.shape)==1):
	#If wt is not 1-d array, use the first element of wt as the latitude weights (as they are repeated anyway)
	if(len(wt.shape)!=1):
	   wt = wt[0]   
	#How to ensure weights at "masked" positions are not summed? Weights don't have a mask but obs and ref do have masks
	#to do this, we compute wt*((obs+ref)/(obs+ref)), this will ensure that the result contains a mask over missing values of x or y.	   
	wtSum = MV.sum(wt*((obs+ref)/(obs+ref)))
	MSE = (MV.sum(wt*MV.power((obs-ref),2))/wtSum).getValue()
    #2-d matrix	
    else:
	#How to ensure weights at "masked" positions are not summed? Weights don't have a mask but obs and ref do have masks
	#to do this, we compute wt*((obs+ref)/(obs+ref)), this will ensure that the result contains a mask over missing values of x or y.
	wtSum = MV.sum(MV.sum(wt*((obs+ref)/(obs+ref))))
	MSE = (MV.sum(MV.sum(wt*MV.power((obs-ref),2)))/wtSum).getValue()
    return MSE    
        
def computeRMSEonMeanCentered(obs,ref):
    '''
    returns the RMSE between the mean-centered obs and ref input lists
    '''
    if(obs.shape!=ref.shape):
        raise '#Dimension of input lists donot match'
	
    obs_mean = getMean(obs)
    ref_mean = getMean(ref)
    E_prime=0
    #1-d vector
    if(len(obs.shape)==1):
        RMSE = (MV.sum(MV.power((obs-obs_mean)-(ref-ref_mean),2))/MV.count(obs))**0.5
    #2-d matrix	
    else:
        RMSE = (MV.sum(MV.sum(MV.power(((obs-obs_mean)-(ref-ref_mean)),2)))/MV.count(obs))**0.5
    return RMSE  
   