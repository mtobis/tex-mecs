'''
Srivatsan Ramanujam(vatsan@cs.utexas.edu)
Python wrapper to the NCAR Ensemble Averager
Given a folder that contains the output of a camrun, it calculates the seasonal and annual averages from the monthly data.
'''

#Usage Example: python ensemble_averager.py $WORK/ch.1/BSI_CAM_0000/ camrun.bsi.cam2.h0. 0020 1

import os
NUM_MONTHS=12

def main(inputFolder,BASE_NAME,BASE_YEAR,num_years):

    num_years = int(num_years)
    currdir = os.getcwd()
    os.chdir(inputFolder) 
    
    '''Ensemble Averaging begins '''
    #We need one DJF,MAM,JJA,SON,ANN file averaged over all years
    for season in ['DJF','MAM','JJA','SON','ANN']:
        season_filelist = []
        for year in range(num_years):
            #Files to be included for DJF seasonal average
	    if(season=='DJF'):
	        f1 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year)+'-'+'12'+'.nc'
		f2 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'01'+'.nc'
		f3 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'02'+'.nc'
		season_filelist.extend([f1,f2,f3])		
                for fnam in [f1,f2,f3]: assert os.path.isfile(fnam)
		
            #Files to be included for MAM seasonal average		
	    if(season=='MAM'):
	        f1 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'03'+'.nc'
		f2 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'04'+'.nc'
		f3 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'05'+'.nc'	            
		season_filelist.extend([f1,f2,f3])
                for fnam in [f1,f2,f3]: assert os.path.isfile(fnam)
		
            #Files to be included for JJA seasonal average	    
	    if(season=='JJA'):
	        f1 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'06'+'.nc'
		f2 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'07'+'.nc'
		f3 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'08'+'.nc'	    
		season_filelist.extend([f1,f2,f3])		
                for fnam in [f1,f2,f3]: assert os.path.isfile(fnam)
		
            #Files to be included for SON seasonal average	    
	    if(season=='SON'):
	        f1 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'09'+'.nc'
		f2 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'10'+'.nc'
		f3 = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+'11'+'.nc'	    
		season_filelist.extend([f1,f2,f3])
                for fnam in [f1,f2,f3]: assert os.path.isfile(fnam)
				
            #Files to be included for ANN seasonal average	    
	    if(season=='ANN'):
	        for month in range(NUM_MONTHS):
                    if month+1 < NUM_MONTHS:
		        f = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+"%02d"%(month+1)+'.nc'
                    else:  
		        f = BASE_NAME + "%04d"%(int(BASE_YEAR)+year)+'-'+"%02d"%(month+1)+'.nc'
	    	    season_filelist.append(f)
            
	#Execute the ncea command to compute the ensemble average.
	#The arguments for ncea are a collection of files which need to be averaged, where the last argument specifies the name of the averaged 
	#output file
	output_file = BASE_NAME + "%04d"%(int(BASE_YEAR)+year+1)+'-'+season+'.nc'
	ncea_cmd = 'ncea '+' '.join(season_filelist)+' '+output_file
	print 'command : ',ncea_cmd
	os.system(ncea_cmd)
				   
    ''' Done Ensemble Averaging '''	
    print '\n Completed computing of seasonal and annual averages of the monthly data'
    os.chdir(currdir)

if(__name__=='__main__'):
    from sys import argv
    if(len(argv)!=5):
       print 'Usage: python ensemble_averager.py <folder containing camrun monthly files> <model file prefix> <base-year> <no. years>'
    else:
       main(argv[1],argv[2],argv[3],argv[4])
       
