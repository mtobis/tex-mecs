# Adapted for numpy/ma/cdms2 by convertcdms.py
'''
----------------------------------
costwrapper_production.py
Srivatsan Ramanujam, Mar 17, 2009
vatsan@cs.utexas.edu
----------------------------------
This is the production version of costwrapper.py
Acts as a wrapper over costcalc.py. This program extracts the necessary variables from the model and reference files, extracts the required fields
from the same and computes the taylor metric for each parameter of interest.
'''

import cdms2 as cdms,MV2 as MV,os,numpy.oldnumeric.ma as MA,sys
from costcalc import calcCost
from costutils import *

#Global variables
dir_sep = '/'
model_dir = ''
ref_dir = ''

allvars = None
allrefs = None

def computeCost(season,param,lastyr,allvars=[]):
    '''
    Computes cost when 12 months worth of model data is provided
    
    '''    
    global model_dir,ref_dir

 
    model_base_name = 'camrun.bsi.cam2.h0.%04d'%int(lastyr)
    #model_file = model_dir+dir_sep+model_dir.split(dir_sep)[-1]+'_'+season+'_climo.nc'
    model_file = model_dir + dir_sep + model_base_name+'-'+season+'.nc'
    if(model_dir==ref_dir):
        model_file = model_dir+dir_sep+param['ref']+'_'+season+'_climo.nc'
    ref_file = ref_dir+dir_sep+param['ref']+'_'+season+'_climo.nc'
    #Check if both model_file and ref_file are present, else skip this iteration of the loop
    f1=None;f2=None
    
    
    try:
       f1 = open(model_file,'r')
       f1.close()
       f2 = open(ref_file,'r')
       f2.close()
    except IOError:
       print 'Could not open either:'+model_file+' or '+ref_file
       return None
    
    model_var,ref_var,wt = extractFields(model_file,ref_file,param)   

    model_var.id = param["id"]
    model_var.name = param["id"] 

    allvars.write(model_var)
    # add model_var to netcdf file

    cost = computeTaylorMetric(model_var,ref_var,wt)
    return cost    
    
def readAndCacheStdDev(stdFile):
    '''
      Reads the std_dev file which has lines of the form  param(field) <tab> season <tab>  std_dev over 20 year 
      and returns this information as a dict
    '''  
    season_param_std={}
    fl = open(stdFile,'r')
    for line in fl:
        line = line.replace('\n','').replace('\r','').replace('std','')
	line = line.split(':')
	line = [k.strip() for k in line]
	#Input is of the form <season> <param> <std>
	if(season_param_std.has_key(line[0])):
	   season_param_std[line[0]][line[1]]=float(line[2])
	else:
	   param = {}
	   param[line[1]]=float(line[2])
	   season_param_std[line[0]]=param
    return season_param_std	   
	
def writeCostsToFile(costs,outFile):
    '''
       Writes the computed costs to an output tab separated file        
    '''
    fl = open(outFile,'w')
    meanCost = 0.0
    count=0.0
    for year in costs.keys():
      for season  in costs[year].keys():
        for  param in costs[year][season].keys():
	     cost = costs[year][season][param]['MSE']
	     meanCost+=cost
	     count+=1
	     line = season+'\t'+param+'\t'+str(costs[year][season][param]['MSE'])+'\n'
	     fl.write(line)
    #Now write the mean cost as well
    line = 'MeanCost'+'\t'+str(meanCost/count)+'\n'
    fl.write(line)
    fl.close()
    
    #Write the mean cost to result.dat
    total = file("result.dat","w")
    total.write(str(meanCost/count)+"\n")
    total.close()
    #sys.stdout.write(str(meanCost/count))

def saveModelandRef(model_var,model_var_name,ref_var,ref_var_name,directory):
    currDir = os.getcwd()
    os.chdir(directory)
    #Write the model and reference variable
    f1 = cdms.open(model_var_name,'w')
    f1.write(model_var)
    f1.close()
    f2=cdms.open(ref_var_name,'w')
    f2.write(ref_var)
    f2.close()
    os.chdir(currDir)

def computeTaylorMetric(model_var,ref_var,wt):
    '''
    Calls the actual method that computes the taylor metric and passes on the same
    '''
    taylor_metric = calcCost(model_var,ref_var,wt)
    return taylor_metric
    
def getPress(var_arr,param,model_level): 
    if(model_level!=0):
       A=var_arr['hyai']
       B=var_arr['hybi']
       Po = 10000
       if(var_arr['P0']!=None):
          Po=var_arr['P0'].getValue()
       
       PS=var_arr['PS']
       if(len(PS.shape)>2):
          PS = PS[0]
       #Reference file might already have the value of the parameter at the required pressure level
       if(var_arr[param].getAxis(1).isLevel()):
           vals = var_arr[param].getAxis(1).getValue().tolist()
	   try:
	   	index = vals.index(model_level)
	   except ValueError:
		var_val = linearInterpolationAtLevel(var_arr[param],PS,A,B,Po,model_level)
		return  var_val
           return  var_arr(param,lev=model_level) 
	   
       var_val = linearInterpolationAtLevel(var_arr[param],PS,A,B,Po,model_level)
       return var_val
       
    lat = var_arr[param].getAxis(2)   
    lon = var_arr[param].getAxis(3)
    P0 = 10000
    if(var_arr['P0'] is not None):
       P0 = var_arr['P0'].getValue()
    PS = var_arr['PS'][0] #Reduce rank by one, eliminating the single time dimension
    hyai = var_arr['hyai']
    hybi = var_arr['hybi']
    return getPressureAvg(var_arr[param],lat,lon,P0,PS,hyai,hybi)


def extractFields(model_file,ref_file,param):
    '''
    Extract the fields of interest from the model and reference files and compute the taylor metric on them after suitable
    pre-processing (which involves linear interpolation of the grids to make them of the same resolution)
    '''

    model_var = cdms.open(model_file,'r')
    ref_var = cdms.open(ref_file,'r')
    

    #Latitude region to extract
    lats = (float(param['latS']),float(param['latN']))
    #Longitude region to extract
    lons = (float(param['lonL']),float(param['lonR']))
    
    model_id = param['id']
    model_level = float(param['lev'])
    ref_level = float(param['lev'])
    
	
    #If the original param['id'] is not present in the file, it is likely to be under a different name, hence try the other possibility	
    #import pdb; pdb.set_trace()
   
    print model_var[param['id']] 
    print type(model_var[param['id']])
        

    if type(model_var[param['id']])!=type(None): # tiresome hack - mt
        model_id = param['id']
    
	
    ref_id = param['id']
    
    if(ref_id == 'TREFHT'):
        ref_id = 'TREFHT_LAND'
	
    if(ref_id == 'STRESS'):
        ref_id = 'STRESS_MAG'
	

    #If the original param['id'] is not present in the file, it is likely to be under a different name, hence try the other possibility	
    
    if type(ref_var[param['id']])!=type(None):
        ref_id = param['id']
    
		
    model_var_val=None;ref_var_val=None;LANDMASK=None;OCEANMASK=None
    
    #****************Obtain ref_var_val first
    if(ref_id in ['RELHUM','U','T']):
        ref_var_val = getPress(ref_var,ref_id,ref_level)
    else:
        ref_var_val = ref_var(ref_id)
	if(ref_id=='PSL' and ref_var[ref_id].units=='Pa'):
	   ref_var_val = ref_var_val*0.01
	   
    if(ref_id == 'TREFHT_LAND'):
        LANDMASK = MA.equal(ref_var_val,-999)
	ref_var_val = MV.array(ref_var_val,mask=LANDMASK)	   

    #STRESS_MAG in the reference is already a masked variable, so extract its mask		   
    if(ref_id =='STRESS_MAG'):
        OCEANMASK = ref_var_val[0]
	
	
    #LHFLX : in the observation data, this variable is already masked (Defined over ocean alone) -- find the right missing value and create the mask
    #Seems to have a problem in understanding the mask
    if(ref_id == 'LHFLX'):
        OCEANMASK = MA.equal(ref_var_val[0],32766)	
	     
    #*************************************** Extracting Model Variable ************************************************************************
    if(model_id=='PRECT'):
    	 model_var_val1 = model_var('PRECC')
	 model_var_val2 = model_var('PRECL')
         print type(model_var_val1)
	 #model_var_val = model_var_val1+model_var_val2
	 model_var_val = MV.array(model_var_val1+model_var_val2)
	 #Convert PRECT,PRECC or PRECL in model that is in 'm/s' to the reference units, which is in 'mm/day'
	 if(model_var['PRECC'].units=='m/s'):
	    # 1 metre = 1000 millimetres
	    # 1 second = 1/(3600*24) days
	    # 1m/s = 1000*3600*24 mm/day
	    model_var_val = model_var_val*(1000.0*3600.0*24)	 
	 
	 #Regrid To Ensure both model and observed data are in the same resolution	    
         print "SG1"
         model_var_val,ref_var_val = spatialRegridding(model_var_val,ref_var_val)
	 
	 
	 #land rainfall
	 if(param['ls']=='1'):
	     LANDMASK = MA.less(model_var['LANDFRAC'],1.0)
	     model_var_val = MV.array(model_var_val,mask=LANDMASK)
	     model_var_val = model_var_val(lat=lats,lon=lons)
	 else:
	     OCEANMASK = MA.less(model_var['OCNFRAC'],1.0)
	     model_var_val = MV.array(model_var_val,mask=OCEANMASK)
	     model_var_val = model_var_val(lat=lats,lon=lons)
	     
    elif(model_id=='LHFLX'):
         #Regrid the model to the dimension of the reference (although reference is of higher resolution, we need to do this coz
	 #the mask is defined for the reference field and not for the model field)
	 model_var_val = model_var(model_id)[0]
	 ref_var_val = ref_var_val[0]
         print "SG2"
         model_var_val,ref_var_val = spatialRegridding(model_var_val,ref_var_val,1)


	 #Apply the ocean mask for the model
	 model_var_val = MV.array(model_var_val,mask=OCEANMASK)
	 model_var_val = model_var_val(lat=lats,lon=lons)
	 	     	         
    elif(model_id=='STRESS'):
         TAUX = model_var('TAUX')
	 TAUY = model_var('TAUY')
	 model_var_val = (TAUX**2+TAUY**2)**0.5
	 
	 #To Ensure both model and observed data are in the same resolution	    
         print "SG3"
         print "SG3"
         model_var_val,ref_var_val = spatialRegridding(model_var_val,ref_var_val)
	 model_var_val = model_var_val[0]
	 #Pacific surface stress: To be considered along only ocean points
	 
	 ##************** BE CAREFUL HERE... Directly givin the OCEANMASK obtained from the reference file appears to create an erroneous mask
	 model_var_val = MV.array(model_var_val,mask=OCEANMASK)
	 model_var_val = model_var_val(lat=lats,lon=lons)
    else:
         #Need to be called before any call to lat,lon
         #Level 0 indicates the weighted average over all pressure levels
         if(model_id in ['RELHUM','U','T']):
             model_var_val = getPress(model_var,model_id,model_level)
	 else:
             model_var_val = model_var(model_id)
	     
	 #Land temperature
	 if(model_id=='TREFHT'):
	    model_var_val = model_var_val[0]
	    
	    #In this case, the reference file already has "TREFHT_LAND", with fill value "-999", 
	    #   1)so we extract the mask out of the ref file and apply the mask on the reference file
	    #   2)Regrid the model to the resolution of the reference file
	    #   3)We apply the mask on the model file

	    #To Ensure both model and observed data are in the same resolution	    
	    #Note we pass the last argument as a "1" to over-ride the default regridding rule (which regrids from high resolution to low)
            print "SG4"
            model_var_val,ref_var_val = spatialRegridding(model_var_val,ref_var_val,1)    
	    model_var_val = MV.array(model_var_val,mask=LANDMASK)
            model_var_val = model_var_val(lat=lats,lon=lons)     
	 else:   
	    #To Ensure both model and observed data are in the same resolution	    
            print model_id
            print "SG5"
            model_var_val,ref_var_val = spatialRegridding(model_var_val,ref_var_val)
	    model_var_val = model_var_val(lat=lats,lon=lons)
	 
	 	 
	 #Convert pressure in PS to millibars, so that it matches with units for PS in ERA40
	 #1 millibar = 100 Pascals
	 if(model_id=='PSL' and model_var[model_id].units=='Pa'):
	    model_var_val = model_var_val*0.01
	    
	    	 	    
    #************************************************** End of Model Variable ************************	    

	    
    #******************************************** Extract REF VARIABLE **************************************    
    #  Special cases 1) STRESS (Pacific Surface Stress needs to be computed only over ocean points 
    #                3) PRECT  (land rainfall if ls=1, 
    #                4) ocean rainfall if ls=0)
    #************************************************************************************************

    if(ref_id=='PRECT'):
        if(param['ls']=='1'):
	    ref_var_val = MV.array(ref_var_val,mask=LANDMASK)
            ref_var_val = ref_var_val(lat=lats,lon=lons)
	else:
	    ref_var_val = MV.array(ref_var_val,mask=OCEANMASK)
	    ref_var_val = ref_var_val(lat=lats,lon=lons)		    
    else:
        ref_var_val = ref_var_val(lat=lats,lon=lons)	    
	             
    #Adjust the shapes
    model_var_val,ref_var_val = adjustShapes(model_var_val,ref_var_val)

    #Apply the weights?
    wt_model,wt_ref = getWeights(model_var_val,ref_var_val)
    # MSE = sum(wt*(model-ref)^2)/sum(wt)
    # Make the weights "conform" to the dimensions of model_var_val and ref_var_val, this is done by copying the weights array, along
    # the longitude dimension
    lon_size = model_var_val.shape[1]
    wt_model = transposeAndConform(wt_model,lon_size)
    wt_ref = transposeAndConform(wt_ref,lon_size)  
    #Since model and reference have been reduced to the same resolution, they should have same latitude weights?
    #So why use even 2?
    
    model_var.close();ref_var.close();del(model_var);del(ref_var)
    
    return model_var_val,ref_var_val,wt_ref
    
def adjustShapes(model_var_val,ref_var_val):
    ###Adjust Shapes
    if(len(model_var_val.shape)==3):
       model_var_val = model_var_val[0]
    if(len(model_var_val.shape)==4):
       model_var_val = model_var_val[0][0]

    if(len(ref_var_val.shape)==3):
       ref_var_val = ref_var_val[0]
    if(len(ref_var_val.shape)==4):
       ref_var_val = ref_var_val[0][0]       
    ###End-Adjust Shapes     
    return model_var_val,ref_var_val
        
def getGlobalRadiativeBalance(year=0,BASE_YEAR=19):
    '''
    Computes the cost for the parameter - Global Radiative Balance
    '''
    #BASE_YEAR=19
    global model_dir
    RAD_BAL_REF = 0.5
    model_base_name = 'camrun.bsi.cam2.h0.'
    #model_file = model_dir + dir_sep + model_base_name+str(BASE_YEAR+year)+'-'+'ANN'+'.nc'
    model_file = model_dir + dir_sep + model_base_name+"%04d"%(BASE_YEAR+year)+'-'+'ANN'+'.nc'
    
    try:  
       f1 = open(model_file,'r')
    except IOError:
       print 'Could not open',model_file
       
    var = cdms.open(model_file,'r')
    model_var = var['FSNT'][0] - var['FLNT'][0]
    # Get the weights and
    # Make the weights "conform" to the dimensions of model_var_val and ref_var_val, this is done by copying the weights array, along
    # the longitude dimension
    model_wt = getModelWeights(model_var)    
    lon_size = model_var.shape[1]
    model_wt = transposeAndConform(model_wt,lon_size)
    
    mean_var = model_wt*model_var
    #Compute the weighted mean
    cost_local={}
    mean_var = MV.sum(MV.sum(mean_var))/MV.sum(MV.sum(model_wt))
    cost_local['MSE'] = (mean_var - RAD_BAL_REF)*(mean_var-RAD_BAL_REF)
    
    '''    
    #Ref var is just a matrix of 0.5 as the value in all cells. To ensure its axis information etc is the same as that
    #of the model_var, just divide model_var with itself and multiply with the scalar 0.5    
    
    ref_var = (model_var/model_var)*RAD_BAL_REF
    ###Directly compute taylor metric
    diff = model_var*model_wt
    cost = computeTaylorMetric(model_var,ref_var,model_wt)
    '''
    var.close()
    return cost_local  
       
def main(model,ref,std_file,lastyr,outFile):
    global model_dir
    global ref_dir
    model_dir = model
    ref_dir = ref
    '''
    The main method simply makes a call to the other methods in this program
    '''
    seasons = ['DJF','MAM','JJA','SON']

    '''
    ----------------------------------------------------------------------------------------------------------------
     Case    , Reference    ,Unit ,   latS , latN, lonL, lonR,  ls(1=lnd,0=ocn), lev, label
    "PSL"    , "ERA40"   ,"1.0",   "-30", "30",  "0","360",   "-1", "0"    , "Sea Level Pressure (ERA40)"
    "SWCF"   , "CERES2"  ,"1.0",   "-30", "30",  "0","360",     "-1", "0"    , "SW Cloud Forcing (CERES2)"
    "LWCF"   , "CERES2"  ,"1.0",   "-30", "30",  "0","360",     "-1", "0"    , "LW Cloud Forcing (CERES2)"
    "PRECT"  , "GPCP"  ,pUnit,   "-30", "30",  "0","360",      "1",  "0"    , "Land Rainfall (30N-30S, GPCP)"
    "PRECT"  , "GPCP"  ,pUnit,   "-30", "30",  "0","360",      "0",  "0"    , "Ocean Rainfall (30N-30S, GPCP)"
    "TREFHT" , "WILLMOTT" ,"1.0",   "-30", "30",  "0","360",   "1",  "0"    , "Land 2-m Temperature (Willmott)"
    "STRESS" , "ERS"  ,"-1.0",  "-5",  "5",   "135","270",     "0",  "0"    , "Pacific Surface Stress (5N-5S,ERS)"
    "U"      , "ERA40"     ,"1.0",   "-30", "30",  "0","360",  "-1", "300"  , "Zonal Wind (300mb, ERA40)"
    "RELHUM" , "ERA40" ,"1.0",  "-30", "30",  "0","360",       "-1", "0"   , "Relative Humidity (ERA40)"
    "T"      , "ERA40"      ,"1.0",  "-30", "30",  "0","360",  "-1", "0"    , "Temperature (ERA40)"
    ----------------------------------------------------------------------------------------------------------------
    '''
    # The parameters of interest are stored as a list of dictionaries
    param0 = {'id':'PSL','ref':'ERA40','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'0','label':'Sea Level Pressure (ERA40)'}
    param1 = {'id':'SWCF','ref':'CERES2','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'0','label':'SW Cloud Forcing (CERES2)'}
    param2 = {'id':'LWCF','ref':'CERES2','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'0','label':'LW Cloud Forcing (CERES2)'}
    param3 = {'id':'PRECT','ref':'GPCP','unit':'pUnit','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'1','lev':'0','label':'Land Rainfall (30N-30S, GPCP)'}
    param4 = {'id':'PRECT','ref':'GPCP','unit':'pUnit','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'0','lev':'0','label':'Ocean Rainfall (30N-30S, GPCP)'}
    param5 = {'id':'TREFHT','ref':'WILLMOTT','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'1','lev':'0','label':'Land 2-m Temperature (Willmott)'}
    param6 = {'id':'STRESS','ref':'ERS','unit':'-1.0','latS':'-5','latN':'5','lonL':'135','lonR':'270','ls':'0','lev':'0','label':'Pacific Surface Stress (5N-5S,ERS)'}
    param7 = {'id':'U','ref':'ERA40','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'300','label':'Zonal Wind (300mb, ERA40)'}
    param8 = {'id':'RELHUM','ref':'ERA40','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'0','label':'Relative Humidity (ERA40)'}
    param9 = {'id':'T','ref':'ERA40','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'-1','lev':'0','label':'Temperature (ERA40)'}
    param10= {'id':'LHFLX','ref':'WHOI','unit':'1.0','latS':'-30','latN':'30','lonL':'0','lonR':'360','ls':'0','lev':'0','label':'Latent Heat Fluxes(WHOI)'}
    
    #Disregarding "STRESS", as it has very low variance..besides it is believed to be highly correlated with sea temperature.
    param_list = [param0,param1,param2,param5,param8,param9,param10] 
    #param_list = [param0,param1,param2,param3,param4,param5,param6,param7,param8,param9,param10]  
    #param_list=[param0,param1]
    #param_list = [param0,param1,param2,param3,param4,param5,param6,param8,param9] 
    #param_list = [param0,param1,param2,param5,param6,param8,param9]  
    numYears =  1
    
    cost_dict = {}
    #compute cost for every year
    for year in range(numYears):      
        #Compute cost for every season
	cost_dict_season = {}
        for season in seasons:
            #print season
            variables = cdms.open("%s.nc"%season,"w")
            cost_dict_param = {}
            for param in param_list:
                #print param["id"]
                cost = computeCost(season,param,lastyr,variables)
	        cost_dict_param[param['label']] = cost
            cost_dict_season[season] = cost_dict_param
            variables.close()

	cost_dict[str(year)] = cost_dict_season
	
   
    #Now obtain the variance from the saved file
    std_cost = readAndCacheStdDev(std_file)

    
    #Divide the MSE entry for  each [year,season,param] by the standard over 20 years
    for year in cost_dict.keys(): 
       print year
       for season in cost_dict[year].keys():
          print season
          for param in cost_dict[year][season].keys():
              print param
	      if(cost_dict[year][season][param].has_key('MSE')):
	          if(not std_cost[season][param]==0):
	              cost_dict[year][season][param]['MSE']/=std_cost[season][param]
      
    
    ## Include the entry for GLOBAL RADIATIVE BALANCE HERE!
    radbal_mse = {}
    radbal_mse['MSE'] = getGlobalRadiativeBalance(BASE_YEAR=int(lastyr))['MSE']
    radbal_mse['MSE'] /= (1.0/4.0) #Such that we assume (1/4.0) to be standard deviation
    if(cost_dict[year].has_key('ANN')):
    	cost_dict[year]['ANN']['RADBAL'] = radbal_mse
    else:
        newfield={}
	newfield['RADBAL'] = radbal_mse
        cost_dict[year]['ANN'] = newfield
    ## End of Global Radiative Balance Code		      
    
    #Write costs to an output file and the mean cost stdout       
    writeCostsToFile(cost_dict,outFile)

if(__name__=='__main__'):
    from sys import argv
    if(len(argv)!=6):
        raise 'Usage: /disk/cg2/dmitri/CDAT/bin/python costwrapper_production.py <model directory> <reference directory>  <std_dev file> <lastyr> <outFile>'
    else:
        main(argv[1],argv[2],argv[3],argv[4],argv[5])
	
