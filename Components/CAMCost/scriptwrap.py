#! /usr/bin/env python
from os.path import isfile
from os import system as S
from os import environ

if isfile("../../../BOGUS"):
    san = environ["SAN"]
    S("cp %s/sampleout/* ." % san)
    S("python ../../../ensemble_averager.py . camrun.bsi.cam2.h0. 0021 1")
    S("python ../../../getcost.py . /work/utexas/ig/tobis/observations" +
      " ../../../std_season_fields.txt costs")
else:
    S("qsub <./run_cam")

