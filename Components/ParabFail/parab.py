def paraboid(x, y, a = 1.0, b= 1.0, c = 2.0, d= 2.0, noise = None):
    """
    >>> for i in range(20):
    ...     print paraboid(-5. + 0.5 * i, 1)
    134.0
    114.75
    97.0
    80.75
    66.0
    52.75
    41.0
    30.75
    22.0
    14.75
    9.0
    4.75
    2.0
    0.75
    1.0
    2.75
    6.0
    10.75
    17.0
    24.75
    """
    if not noise:
        noise = lambda: 0
    return(a * (x-b)**2 + c*(x-d)**2 + noise())

def mynoise(noisamp):
    from random import gauss
    return lambda : noisamp * gauss(0.,1.)

Verbose = False

if __name__ == "__main__":

    from sys import argv

    if "-t" in argv:
        import doctest
        doctest.testmod()
    else:
        noisename = None
        if len(argv) > 1:
            fnam = argv[1]
        else:
            fnam = "parms.dat"
        parms = file(fnam,"r")

        import json
        from jload import jload
        globals().update(jload(parms))
        if noisename:
            noise = eval(noisename+"(float(noisamp))")
	else:
            noise = None
        result = paraboid(a,b,noise=noise)
        if Verbose: print result
        output = file("result.dat","w") 
	output.write(str(result)+"\n")
	output.close()
     
